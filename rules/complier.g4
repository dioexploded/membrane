grammar complier ;

RAWTYPE : 'bool'
| 'int' 
| 'string'
| 'void' 
;
NUL: 'null' ;
TRUE: 'true' ;
FALSE : 'false' ;
IF : 'if' ;
ELSE : 'else' ;
FOR : 'for' ;
WHILE : 'while' ;
BREAK : 'break' ;
CONTINUE : 'continue' ;
RETURN : 'return' ;
NEW : 'new' ;
CLASS : 'class' ;
THIS : 'this' ;
LeftF : '[' ;
RightF : ']' ;
ID : [a-zA-Z_][a-zA-Z0-9_]* ;
NUM : [0]|([1-9][0-9]*) ;
CONST_STR : '"' (TRANSFERRED | .)*? '"' ;
TRANSFERRED : '\\n' | '\\\\' | '\\"' ;
COMMENT : '//' .*? '\r'?'\n' ->skip ;
WS : [ \t\r\n]+ ->skip ;

type:
(RAWTYPE|ID) (LeftF RightF)*
;

prog:
(delc_var|delc_func|delc_class)*
;

delc_var:
type ID ('=' expr)? ';'
;

exprlist:
expr (',' expr)*
;

delc_only: 
type ID 
;

delc_func:
type ID '(' (delc_only (',' delc_only)*)? ')' '{' block* '}' # delcfunc
;

delc_cons_func:
ID '('')' '{' block* '}'
;

classbody:
((delc_only ';')|delc_func|delc_cons_func)*
;

delc_class:
CLASS ID '{' classbody '}' # delcclass
; 


block:
stat #statblock
|'{' block* '}' #newblock 
;

stat:
delc_var # delcstat
| IF '(' expr ')' block (ELSE block)? # ifstat
| WHILE '(' expr ')' block # whilestat
| FOR '(' ucexpr ';' ucexpr ';' ucexpr ')' block # forstat 
| RETURN expr? ';' # returstat
| BREAK ';'# breakstat
| CONTINUE ';'# continuestat
| expr ';' # exprstat
| ';' #emptystat
;

expr: 
 NUM #num
|TRUE #true
|FALSE #false
|NUL #null
|CONST_STR #conststr
|THIS #this
|ID #id
|'(' expr ')' #bracket
|expr '.' ID #member
|expr LeftF expr RightF #index
|expr '(' exprlist? ')' #callfunc
|expr op=('++'|'--') #rightselfaddsub
|<assoc=right > '-'expr #negative
|<assoc=right > op=('++'|'--') expr #leftselfaddsub
|<assoc=right > '!' expr #not
|<assoc=right> '~' expr #rev
|expr op=('*'|'/'|'%') expr #muldivmod
|expr op=('+'|'-') expr #addsub
|expr op=('<<'|'>>') expr #shift
|expr op=('<'|'>'|'>='|'<=') expr #partial
|expr op=('=='|'!=') expr #compare
|expr '&' expr #and
|expr '^' expr #xor
|expr '|' expr #or
|expr '&&' expr #logicand
|expr '||' expr #logicor
|NEW (RAWTYPE|ID) (LeftF expr? RightF)* #new
|<assoc=right > expr '=' expr #assign
;

ucexpr:

|expr
;






