#pragma once
#include<complierBaseVisitor.h>
#include<vector>
#include<unordered_map>
#include<string>
#include<map>
#include<stack>
using namespace std;
enum vartype { nul, imm, regist, label };
enum etype { nop, val, addr };
class reg
{
public:
	int id;
	reg(int pid)
		:id(pid)
	{

	}
	reg(const reg & o)
	{
		id = o.id;
	}
};
class var
{
public:
	vartype type;
	long long val;
	reg *r;
	string l;
	var()
		:type(vartype::nul), val(0), r(nullptr), l()
	{

	}
	var(long long pval)
		:type(vartype::imm), val(pval), r(nullptr), l()
	{

	}
	var(reg *pr)
		:type(vartype::regist), val(0), r(pr), l()
	{

	}
	var(const string & pl)
		:type(vartype::label), val(0), r(nullptr), l(pl)
	{

	}
	var(const var & o)
	{
		type = o.type;
		if (type == imm)
			val = o.val;
		if (type == label)
			l = o.l;
		if (type == regist)
			r = new reg(*o.r);
	}
	string content()
	{
		if (type == vartype::imm)
			return to_string(val);
		else if (type == vartype::regist)
			return "$r" + to_string(r->id);
		else if (type == vartype::label)
			return l;
		else return "nop";
	}
};
class factory
{
public:
	int rid;
	int lid;
	stack<int> s;
	map<int, bool> imp;
	factory()
		:rid(0), lid(0)
	{

	}
	reg *newreg(bool im=false)
	{
		int r;
		if (im)
		{
			r = rid++;
			imp[r] = true;
		}
		else
		{
			if (s.empty())
				r = rid++;
			else
			{
				r = s.top();
				s.pop();
			}
		}
		return new reg(r);
	}
	void rel(var *x)
	{
		if (x->type != regist) return;
		int id = x->r->id;
		if (imp.count(id) && imp[id])
			return;
		s.push(id);
	}
	string newlabel()
	{
		return "l" + to_string(lid++);
	}
};

class inst
{
public:
	virtual void print(ostream & out)
	{
		out << content() << endl;
	}
	virtual string content()
	{
		return string();
	}
};
class delc :public inst
{
public:
	string name;
	var *r;
	var *v;
	delc(const string & pname, var *pr,var *pv)
		:name(pname), r(pr),v(pv)
	{

	}
	delc(const delc & o)
	{
		name = o.name;
		r = new var(*o.r);
	}
	string content() override
	{
		string ret = name + " is defined.Address is saved in " + r->content();
		if (v) ret += ". Value is saved in" + v->content();
		return ret;
	}
};
class lab :public inst
{
public:
	var *l;
	lab(var *pl)
		:l(pl)
	{

	}
	lab(const lab& o)
	{
		l = new var(*(o.l));
	}
	string content() override
	{
		return l->content() + ":";
	}
};
class bincalc :public inst
{
public:
	var *rs1, *rs2;
	var *rd;
	bincalc(var *pr1, var *pr2, var *prd)
		:rs1(pr1), rs2(pr2), rd(prd)
	{

	}

	bincalc(const bincalc & o)
	{
		rs1 = new var(*o.rs1);
		rs2 = new var(*o.rs2);
		rd = new var(*o.rd);
	}
};
class Add :public bincalc
{
public:
	Add(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "add " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Sub :public bincalc
{
public:
	Sub(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "sub " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Mult :public bincalc
{
public:
	Mult(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "mult " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Div :public bincalc
{
public:
	Div(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "div " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Mod :public bincalc
{
public:
	Mod(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "mod " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Lshift :public bincalc
{
public:
	Lshift (var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "lshift " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Rshift :public bincalc
{
public:
	Rshift(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "rshift " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Or :public bincalc
{
public:
	Or(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "or " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class And :public bincalc
{
public:
	And(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "and " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Xor :public bincalc
{
public:
	Xor(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "xor " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_base:public bincalc
{
public:
	cmp_base(var *pr1, var *pr2, var *prd)
		:bincalc(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return string();
	}
};
class cmp_LT :public cmp_base
{
public:
	cmp_LT(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_LT " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_LE :public cmp_base
{
public:
	cmp_LE(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_LE " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_EQ :public cmp_base
{
public:
	cmp_EQ(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_EQ " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_GE :public cmp_base
{
public:
	cmp_GE(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_GE " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_GT :public cmp_base
{
public:
	cmp_GT(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_GT " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class cmp_NE :public cmp_base
{
public:
	cmp_NE(var *pr1, var *pr2, var *prd)
		:cmp_base(pr1, pr2, prd)
	{

	}
	string content() override
	{
		return "cmp_NE " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Not :public inst
{
public:
	var *rd;
	var *rs;
	Not(var *prd, var *prs)
		:rd(prd), rs(prs)
	{

	}
	Not(const Not & o)
	{
		rd = new var(*o.rd);
		rs = new var(*o.rs);
	}
	string content() override
	{
		return "Not " + rd->content() + " " + rs->content();
	}
};
class Neg :public inst
{
public:
	var *rd;
	var *rs;
	Neg(var *prd, var *prs)
		:rd(prd), rs(prs)
	{

	}
	Neg(const Neg & o)
	{
		rd = new var(*o.rd);
		rs = new var(*o.rs);
	}
	string content() override
	{
		return "Neg " + rd->content() + " " + rs->content();
	}
};
class Rev :public inst
{
public:
	var *rd;
	var *rs;
	Rev(var *prd, var *prs)
		:rd(prd), rs(prs)
	{

	}
	Rev(const Rev & o)
	{
		rd = new var(*o.rd);
		rs = new var(*o.rs);
	}
	string content() override
	{
		return "Rev " + rd->content() + " " + rs->content();
	}
};
class Load :public inst
{
public:
	var *rd;
	var *rs1, *rs2;
	Load(var *prd, var *pr1, var *pr2)
		:rd(prd), rs1(pr1), rs2(pr2)
	{

	}
	Load(const Load & o)
	{
		rd = new var(*o.rd);
		rs1 = new var(*o.rs1);
		rs2 = new var(*o.rs2);
	}
	string content() override
	{
		return "load " + rd->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Store :public inst
{
public:
	var *rs;
	var *rd1, *rd2;
	Store(var *prs, var *pr1, var *pr2)
		:rs(prs), rd1(pr1), rd2(pr2)
	{

	}
	Store(const Store & o)
	{
		rs = new var(*o.rs);
		rd1 = new var(*o.rd1);
		rd2 = new var(*o.rd2);
	}
	string content() override
	{
		return "store " + rs->content() + " " + rd1->content() + " " + rd2->content();
	}
};
class Move :public inst
{
public:
	var *rs;
	var *rd;
	Move(var *prd, var *prs)
		:rd(prd), rs(prs)
	{
	}
	Move(const Move & o)
	{
		rs = new var(*o.rs);
		rd = new var(*o.rd);
	}
	string content() override
	{
		return "move " + rd->content() + " " + rs->content();
	}
};
class Jump :public inst
{
public:
	var *rs;
	Jump(var *prs)
		:rs(prs)
	{

	}
	Jump(const Jump & o)
	{
		rs = new var(*o.rs);
	}

	string content() override
	{
		return "jump " + rs->content();
	}
};
class Branch :public inst
{
public:
	var *rsc;
	var *rs1, *rs2;
	Branch(var *prsc, var *pr1, var *pr2)
		:rsc(prsc), rs1(pr1), rs2(pr2)
	{

	}
	Branch(const Branch & o)
	{
		rsc = new var(*o.rsc);
		rs1 = new var(*o.rs1);
		rs2 = new var(*o.rs2);
	}
	string content() override
	{
		return "branch " + rsc->content() + " " + rs1->content() + " " + rs2->content();
	}
};
class Return :public inst
{
public:
	var *rs;
	Return(var *prs)
		:rs(prs)
	{

	}
	Return(const Return & o)
	{
		rs = new var(*o.rs);
	}
	string content() override
	{
		return "return " + rs->content();
	}
};
class Call :public inst
{
public:
	var* func;
	vector<var*> args;
	Call(var *pf, const vector<var*> pargs)
		:func(pf), args(pargs)
	{

	}
	Call(const Call & o)
	{
		func = new var(*o.func);
		for (int i = 0; i < o.args.size(); i++)
			args.push_back(new var(*o.args[i]));
	}
	string content() override
	{
		string ret = "call " + func->content();
		for (auto i : args)
			ret += " " + i->content();
		return ret;
	}
};
inline int is_pow(long long x)
{
	int k = 0, t = 1;
	while (t < x)
	{
		t *= 2;
		++k;
	}
	if (t == x)
		return k;
	return -1;
}

extern factory comp;
class prework1 :public complierBaseVisitor
{
public:
	unordered_map<string, string> name2type;
	unordered_map<string, int> memoffsets;
	unordered_map<string, int> classsize;
	unordered_map<string, var*> func2l;
	unordered_map<string, string> func2type;
	unordered_map<string, int> func2n;
	string prefix;
	int offset;
	void ass(string name, const string & type, var* l)
	{
		name2type[name] = "func";
		name = "func_" + name;
		func2type[name] = type;
		func2l[name] = l;
	}
	prework1()
	{
		offset = 0;
		/*name2type["getInt"] = "func";
		func2type["func_getInt"] = "int";
		func2l["func_getInt"] = new var(comp.newlabel());*/
		ass("getInt", "int", new var(comp.newlabel()));
		/*name2type["printInt"] = "func";
		func2type["func_printInt"] = "void";
		func2l["func_printInt"] = new var(comp.newlabel());*/
		ass("printInt", "void", new var(comp.newlabel()));

		func2l["func_new"] = new var("malloc");//malloc

		/*name2type["array.size"] = "func";
		func2type["func_array-size"] = "int";
		func2l["func_array.size"] = new var(comp.newlabel());*/

		ass("array.size", "int", new var(comp.newlabel()));

		//func2l["func_print"] = new var(comp.newlabel());
		//func2l["func_println"] = new var(comp.newlabel());
		//func2l["func_getString"] = new var(comp.newlabel());
		ass("print", "void", new var(comp.newlabel()));
		ass("println", "void", new var(comp.newlabel()));
		ass("getString", "string", new var(comp.newlabel()));
		ass("toString", "string", new var(comp.newlabel()));
		ass("string.length", "int", new var(comp.newlabel()));
		ass("string.substring", "string", new var(comp.newlabel()));
		ass("string.parseInt", "int", new var(comp.newlabel()));
		ass("string.ord", "int", new var(comp.newlabel()));
		ass("string+", "string", new var(comp.newlabel()));
		ass("string<", "bool", new var(comp.newlabel()));
		ass("string<=", "bool", new var(comp.newlabel()));
		ass("string>", "bool", new var(comp.newlabel()));
		ass("string>=", "bool", new var(comp.newlabel()));
		ass("string==", "bool", new var(comp.newlabel()));
		ass("string!=", "bool", new var(comp.newlabel()));
	}
	virtual antlrcpp::Any visitDelcfunc(complierParser::DelcfuncContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		name = prefix + name;
		name2type[name] = "func";
		name = "func_" + name;
		func2type[name] = ctx->type()->getText();
		func2l[name] = new var(comp.newlabel());
		func2n[name] = ctx->delc_only().size();
		if (!prefix.empty()) func2n[name]++;
		return 0;
	}
	virtual antlrcpp::Any visitDelcclass(complierParser::DelcclassContext *ctx) override 
	{
		string classname = ctx->ID()->getText();
		prefix = classname + '.';
		offset = 0;
		visit(ctx->classbody());
		classsize[classname] = offset;
		offset = 0;
		prefix.clear();
		return 0;
	}
	virtual antlrcpp::Any visitClassbody(complierParser::ClassbodyContext *ctx) override 
	{
		for (auto i : ctx->delc_only())
			visit(i);
		for (auto i : ctx->delc_func())
			visit(i);
		for (auto i : ctx->delc_cons_func())
			visit(i);
		return 0;
	}
	virtual antlrcpp::Any visitDelc_only(complierParser::Delc_onlyContext *ctx) override 
	{
		if (prefix.empty()) return 0;
		string type = ctx->type()->getText();
		string name = prefix + ctx->ID()->getText();
		name2type[name] = type;
		/*if (type == "bool")
			memoffsets[name] = offset++;
		else if (type == "int")
		{
			offset += (4 - offset & 3) & 3;
			memoffsets[name] = offset;
			offset += 4;
		}
		else
		{
			offset += (8 - offset & 7) & 7;
			memoffsets[name] = offset;
			offset += 8;
		}*/
		memoffsets[name] = offset;
		offset += 8;
		return 0;
	}
	virtual antlrcpp::Any visitDelc_cons_func(complierParser::Delc_cons_funcContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		name = prefix + name;
		name2type[name] = "func";
		name = "func_" + name;
		func2type[name] = "void";
		func2l[name] = new var(comp.newlabel());
		func2n[name] = 1;
		return 0;
	}
};
typedef int handle;
class basicblock
{
public:
	vector<inst*> stmt;
	vector<handle> to;
	void push_back(inst *t)
	{
		stmt.push_back(t);
	}
	void addto(handle t)
	{
		to.push_back(t);
	}
};
class funcstmt
{
public:
	handle now;
	vector<basicblock> bbs;
	funcstmt()
		:now(-1)
	{

	}
	handle newblock()
	{
		handle ret = bbs.size();
		bbs.push_back(basicblock());
		return ret;
	}
	void chb(handle h)
	{
		if (h == -1) throw "here";
		now = h;
	}
	void push_back(inst *t)
	{
		bbs[now].push_back(t);
	}
	void addto(handle h)
	{
		bbs[now].addto(h);
	}
};
class IRtype
{
	string fname;
public:
	unordered_map<string, funcstmt> fcs;

	IRtype()
	{
		
	}
	void chf(const string & name)
	{
		if (!fcs.count(name))
			fcs[name] = move(funcstmt());
		fname = name;
	}
	void push_back(inst *t)
	{
		fcs[fname].push_back(t);
		return;
	}
	funcstmt& getref()
	{
		return fcs[fname];
	}
};
class IRgen :public complierBaseVisitor
{
public:
	vector<unordered_map<string, string>> name2type;
	vector<unordered_map<string, var*>> name2reg;
	unordered_map<string, int> memoffsets;
	unordered_map<string, int> classsize;
	unordered_map<string, var*> func2l;
	unordered_map<string, string> func2type;
	unordered_map<string, int> func2n;

	vector<string> cstr;
	vector<int> cstr2reg;
	int cid;

	var* vthis;
	string prefix;
	string typenew;
	int rubbishcode;//the layer number of scope;

	bool pump;//is trying to find func

	unordered_map<var*, var*> m2r;
	bool isinfunc;//in func?
	
	IRtype IR;
	var* retreg;

	bool previsit;
	bool side;


	class retype
	{
	public:
		var *r;
		etype t;
		string tn;
		retype(var *pr, etype pt, const string ptn)
			:r(pr), t(pt), tn(ptn)
		{

		}
	};
	typedef pair<var*, handle> loop;
	vector<loop> loopb;
	vector<loop> loope;
	IRgen(const prework1 & pre1)
		:memoffsets(move(pre1.memoffsets)), func2l(pre1.func2l), func2type(pre1.func2type), func2n(pre1.func2n), classsize(pre1.classsize), prefix(), cid(0),typenew()
	{
		retreg = new var(comp.newreg(1));
		//IR.push_back(new delc("$return", retreg));
		name2type.push_back(move(pre1.name2type));
		name2reg.push_back(unordered_map<string, var*>());
		vthis = nullptr;
		rubbishcode = -1;
		pump = false;
		previsit = false;
		side = false;
	}
	
	void push()
	{
		name2type.push_back(move(unordered_map<string, string>()));
		name2reg.push_back(move(unordered_map<string, var*>()));
		return;
	}

	void pop()
	{
		name2type.pop_back();
		name2reg.pop_back();
		return;
	}

	void print(ostream & out)
	{
		for (auto f : IR.fcs)
		{
			out << f.first << ":" << endl;
			auto bbs = f.second.bbs;
			for (int i = 0; i < bbs.size(); i++)
			{
				auto bb = bbs[i];
				out << i << ":" << endl;
				for (auto stmt : bb.stmt)
					out << stmt->content() << endl;
				out << "link to:" << endl;
				for (auto l : bb.to)
					out << l << " ";
				out << endl << endl;
			}
			out << endl;
		}
		for (int i = 0; i < cstr.size();++i)
		{
			out << "_cstr" << i << endl;
			out << "\t" << cstr[i] << endl;

		}
	}

	var *getval(const retype & rt)
	{
		var *ret = nullptr;
		if (rt.t == etype::val)
			ret = rt.r;
		else if (rt.t == etype::addr)
		{
			if (m2r.count(rt.r))
				ret = m2r[rt.r];
			else
			{
				ret = new var(comp.newreg());
				IR.push_back(new Load(ret, rt.r, new var()));
			}
		}
		return ret;
	}

	string getype4name(const string & name)
	{
		rubbishcode = name2type.size() - 1;
		for (auto i = name2type.rbegin(); i != name2type.rend(); ++i,--rubbishcode)
			if (i->count(name))
				return (*i)[name];
		//rubbishcode = -1;
		throw "not found\n";
		return "";
	}

	var *getreg4name(const string & name)
	{
		rubbishcode = name2reg.size() - 1;
		for (auto i = name2reg.rbegin(); i != name2reg.rend(); ++i,--rubbishcode)
			if (i->count(name))
				return (*i)[name];
		throw "not found\n";
		//rubbishcode = -1;
		return nullptr;
	}

	virtual antlrcpp::Any visitProg(complierParser::ProgContext *ctx) override 
	{
		isinfunc = false;
		IR.chf("global");
		funcstmt& g = IR.fcs["global"];
		g.chb(g.newblock());
		g.push_back(new lab(new var("main")));
		visitChildren(ctx);
		IR.chf("global");
		IR.push_back(new Call(func2l["func_main"], vector<var*>()));
		IR.push_back(new Return(new var()));
		return 0;
	}

	virtual antlrcpp::Any visitDelc_var(complierParser::Delc_varContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		name2type.back()[name] = ctx->type()->getText();
		var *t = new var(comp.newreg(1));
		if (isinfunc)
		{
			m2r[t] = new var(comp.newreg(1));
			IR.push_back(new delc(name, t, m2r[t]));
			//IR.push_back(new Load(m2r[t], t, new var()));
		}
		else IR.push_back(new delc(name, t, nullptr));
		name2reg.back()[name] = t;
		if (ctx->expr())
		{
			retype r = visit(ctx->expr());
			var *rs = getval(r);
			if (m2r.count(t))
				IR.push_back(new Move(m2r[t], rs));
			else IR.push_back(new Store(rs, t, new var()));
		}
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitDelc_only(complierParser::Delc_onlyContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		name2type.back()[name] = ctx->type()->getText();
		var *t = new var(comp.newreg());
		//IR.push_back(new delc(name, t));
		name2reg.back()[name] = t;
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitDelcfunc(complierParser::DelcfuncContext *ctx) override
	{
		string fname = ctx->ID()->getText();
		if (pump && !prefix.empty())
		{
			name2type.back()[fname] = "func";
			return 0;
		}
		fname = "func_" + prefix + fname;
		IR.chf(fname);
		auto &fc = IR.getref();
		fc.chb(fc.newblock());
		var *l = func2l[fname];
		IR.push_back(new lab(l));
		m2r.clear();
		isinfunc = true;
		push();
		vector<inst*> temp;
		if (!prefix.empty())
		{
			string type = prefix;
			type.pop_back();
			var *t = new var(comp.newreg(1));
			name2type.back()["_vthis"] = type;
			name2reg.back()["_vthis"] = t;
			m2r[t] = new var(comp.newreg(1));
			IR.push_back(new delc("_vthis", t, m2r[t]));
			temp.push_back(new Load(m2r[t], t, new var()));
		}
		for (auto i : ctx->delc_only())
		{
			string name = i->ID()->getText();
			name2type.back()[name] = i->type()->getText();
			var *t = new var(comp.newreg(1));
			name2reg.back()[name] = t;
			m2r[t] = new var(comp.newreg(1));
			IR.push_back(new delc(name, t, m2r[t]));
			temp.push_back(new Load(m2r[t], t, new var()));
		}
		for (auto s : temp)
			IR.push_back(s);
		for (auto i : ctx->block())
			visit(i);
		pop();
		isinfunc = false;
		m2r.clear();
		IR.push_back(new Return(new var()));
		IR.chf("global");
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitDelcclass(complierParser::DelcclassContext *ctx) override 
	{
		string classname = ctx->ID()->getText();
		prefix = classname + ".";
		push();
		visit(ctx->classbody());
		prefix.clear();
		pop();
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitClassbody(complierParser::ClassbodyContext *ctx) override 
	{
		pump = true;
		for (auto i : ctx->delc_func())
			visit(i);
		pump = false;
		for (auto i : ctx->delc_only())
			visit(i);
		for (auto i : ctx->delc_func())
			visit(i);
		for (auto i : ctx->delc_cons_func())
			visit(i);
		return 0;
	}

	virtual antlrcpp::Any visitStatblock(complierParser::StatblockContext *ctx) override 
	{
		//push();
		visit(ctx->stat());
		//pop();
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitNewblock(complierParser::NewblockContext *ctx) override 
	{
		push();
		for (auto i : ctx->block())
			visit(i);
		pop();
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitNull(complierParser::NullContext *ctx) override 
	{
		if (previsit)
			return string("null");
		return retype(new var(0LL), etype::val, "null");
	}

	virtual antlrcpp::Any visitNum(complierParser::NumContext *ctx) override 
	{
		if (previsit)
			return string("int");
		int val = atoi(ctx->NUM()->getText().c_str());
		return retype(new var(val), etype::val, "int");
	}

	virtual antlrcpp::Any visitFalse(complierParser::FalseContext *ctx) override 
	{
		if (previsit)
			return string("bool");
		return retype(new var(0LL), etype::val, "bool");
	}

	virtual antlrcpp::Any visitTrue(complierParser::TrueContext *ctx) override
	{
		if (previsit)
			return string("bool");
		return retype(new var(1LL), etype::val, "bool");
	}

	virtual antlrcpp::Any visitId(complierParser::IdContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		string type = getype4name(name);
		if (previsit)
			return type;

		if (rubbishcode==0 && type == "func")
			return retype(new var(), etype::nop, "func_" + name);
		if (rubbishcode == 1 && !prefix.empty())
		{
			if (type != "func")
			{
				var *r = new var(comp.newreg());
				var *l = getreg4name("_vthis");
				//var *t = new var(comp.newreg());
				//IR.push_back(new Load(t, l, new var()));
				assert(m2r.count(l));
				IR.push_back(new Add(m2r[l], new var((long long)memoffsets[prefix + name]), r));
				return retype(r, etype::addr, type);
			}
			else
			{
				var *l = getreg4name("_vthis");
				//var *rd = new var(comp.newreg());
				assert(m2r.count(l));
				//IR.push_back(new Load(rd, l, new var()));
				vthis = m2r[l];
				return retype(new var(), etype::nop, "func_" + prefix + name);
			}
		}
		return retype(getreg4name(name), etype::addr,type);
	}

	virtual antlrcpp::Any visitThis(complierParser::ThisContext *ctx) override 
	{
		if (previsit)
			return prefix;
		string type = prefix;
		type.pop_back();
		return retype(getreg4name("_vthis"), etype::addr, type);
	}

	virtual antlrcpp::Any visitConststr(complierParser::ConststrContext *ctx) override 
	{
		if (previsit)
			return string("string");
		string c = ctx->getText();
		int l = c.length();
		string t;
		t.resize(l - 2);
		for (int i = 0; i < l - 2; i++) t[i] = c[i + 1];
		var *r = new var(comp.newreg(1));
		cstr2reg.push_back(r->r->id);
		cstr.push_back(t);
		return retype(r, etype::val, "string");
	}

	virtual antlrcpp::Any visitAddsub(complierParser::AddsubContext *ctx) override 
	{
		if (previsit)
		{
			string peek = visit(ctx->expr(0));
			visit(ctx->expr(1));
			if (peek == "string") side = true;
			return peek;
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		if (ctx->op->getText() == "+")
		{
			if (r1.tn == "int")
				IR.push_back(new Add(rs1, rs2, rd));
			else
			{
				vector<var*> args;
				args.push_back(rs1);
				args.push_back(rs2);
				IR.push_back(new Call(func2l["func_string+"], args));
				IR.push_back(new Move(rd, retreg));
			}
		}
		else if (ctx->op->getText() == "-")
			IR.push_back(new Sub(rs1, rs2, rd));
		return ret;
	}

	virtual antlrcpp::Any visitAssign(complierParser::AssignContext *ctx) override 
	{
		if (ctx->expr(0)->getText() == "g_useless[i][j]")
			return retype(nullptr, etype::nop, "");
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rd = r1.r, *rs = getval(r2);
		comp.rel(rs);
		if (m2r.count(rd))
			IR.push_back(new Move(m2r[rd], rs));
		else IR.push_back(new Store(rs, rd, new var()));
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitIndex(complierParser::IndexContext *ctx) override
	{
		if (previsit)
		{
			side = true;
			return string("");
		}

		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		string tn = r1.tn;
		tn.pop_back(); tn.pop_back();
		var *rs2 = getval(r2);
		comp.rel(rs2);
		var *temp = new var(comp.newreg());
		IR.push_back(new Mult(rs2, new var(8LL), temp));
		var *rs1 = getval(r1);
		comp.rel(temp);
		comp.rel(rs1);
		var *rd = new var(comp.newreg());
		IR.push_back(new Add(rs1, temp, rd));
		return retype(rd, etype::addr, tn);
	}

	virtual antlrcpp::Any visitMember(complierParser::MemberContext *ctx) override
	{
		if (previsit)
		{
			side = true;
			return string("");
		}
		retype r = visit(ctx->expr());
		string tn = r.tn;
		if (tn.back() == ']')
			tn = "array";
		tn += "." + ctx->ID()->getText();
		if (!memoffsets.count(tn))
		{
			vthis = getval(r);
			return retype(new var(), etype::nop, "func_" + tn);
		}
		int offset = memoffsets[tn];
		
		var *rs = getval(r);
		comp.rel(rs);
		var *rd = new var(comp.newreg());
		IR.push_back(new Add(rs, new var(offset), rd));
		return retype(rd, etype::addr, getype4name(tn));
	}

	virtual antlrcpp::Any visitLogicor(complierParser::LogicorContext *ctx) override
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("bool");
		}


		previsit = true;
		side = false;
		visit(ctx->expr(0));
		visit(ctx->expr(1));
		previsit = false;
		if (!side)
		{
			retype r1 = visit(ctx->expr(0));
			retype r2 = visit(ctx->expr(1));
			var *rs1 = getval(r1), *rs2 = getval(r2);
			comp.rel(rs1); comp.rel(rs2);
			var *rd = new var(comp.newreg());
			IR.push_back(new Or(rs1, rs2, rd));
			return retype(rd, etype::val, "bool");
		}


		var *rd = new var(comp.newreg());
		auto &fc = IR.getref();
		var *tl = new var(comp.newlabel()), *fl = new var(comp.newlabel()), *el = new var(comp.newlabel());
		var *e2l = new var(comp.newlabel());
		handle th = fc.newblock(), fh = fc.newblock(), eh = fc.newblock();
		handle e2h = fc.newblock();

		retype r1 = visit(ctx->expr(0));
		var *rs1 = getval(r1);
		IR.push_back(new Branch(rs1, tl, e2l));
		comp.rel(rs1);
		fc.addto(th); fc.addto(e2h);

		fc.chb(e2h);
		IR.push_back(new lab(e2l));
		retype r2 = visit(ctx->expr(1));
		var *rs2 = getval(r2);
		IR.push_back(new Branch(rs2, tl, fl));
		comp.rel(rs2);
		fc.addto(th); fc.addto(fh);

		fc.chb(th);
		IR.push_back(new lab(tl));
		IR.push_back(new Move(rd, new var(1LL)));
		IR.push_back(new Jump(el));
		fc.addto(eh);

		fc.chb(fh);
		IR.push_back(new lab(fl));
		IR.push_back(new Move(rd, new var(0LL)));
		IR.push_back(new Jump(el));
		fc.addto(eh);

		fc.chb(eh);
		IR.push_back(new lab(el));

		return retype(rd, etype::val, "bool");
	}

	virtual antlrcpp::Any visitLogicand(complierParser::LogicandContext *ctx) override
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("bool");
		}
		


		previsit = true;
		side = false;
		visit(ctx->expr(0));
		visit(ctx->expr(1));
		previsit = false;
		if (!side)
		{
			retype r1 = visit(ctx->expr(0));
			retype r2 = visit(ctx->expr(1));
			var *rs1 = getval(r1), *rs2 = getval(r2);
			comp.rel(rs1); comp.rel(rs2);
			var *rd = new var(comp.newreg());
			IR.push_back(new And(rs1, rs2, rd));
			return retype(rd, etype::val, "bool");
		}

		var *rd = new var(comp.newreg());
		auto &fc = IR.getref();
		var *tl = new var(comp.newlabel()), *fl = new var(comp.newlabel()), *el = new var(comp.newlabel());
		var *e2l = new var(comp.newlabel());
		handle th = fc.newblock(), fh = fc.newblock(), eh = fc.newblock();
		handle e2h = fc.newblock();

		retype r1 = visit(ctx->expr(0));
		var *rs1 = getval(r1);
		IR.push_back(new Branch(rs1, e2l, fl)); 
		comp.rel(rs1);
		fc.addto(e2h); fc.addto(fh);
		
		fc.chb(e2h);
		IR.push_back(new lab(e2l));
		retype r2 = visit(ctx->expr(1));
		var *rs2 = getval(r2);
		IR.push_back(new Branch(rs2, tl, fl));
		comp.rel(rs2);
		fc.addto(th); fc.addto(fh);

		fc.chb(th);
		IR.push_back(new lab(tl));
		IR.push_back(new Move(rd, new var(1LL)));
		IR.push_back(new Jump(el));
		fc.addto(eh);
		
		fc.chb(fh);
		IR.push_back(new lab(fl));
		IR.push_back(new Move(rd, new var(0LL)));
		IR.push_back(new Jump(el));
		fc.addto(eh);

		fc.chb(eh);
		IR.push_back(new lab(el));

		
		
		return retype(rd, etype::val, "bool");
	}

	virtual antlrcpp::Any visitRightselfaddsub(complierParser::RightselfaddsubContext *ctx) override 
	{
		if (previsit)
		{
			side = true;
			return string("");
		}
		retype r = visit(ctx->expr());
		var *v = getval(r);
		var *rd = new var(comp.newreg());
		IR.push_back(new Move(rd, v));
		comp.rel(v);
		var *temp = new var(comp.newreg());
		if(ctx->op->getText()=="++")
			IR.push_back(new Add(v, new var(1LL), temp));
		else if(ctx->op->getText() == "--")
			IR.push_back(new Sub(v, new var(1LL), temp));


		comp.rel(temp);
		if (m2r.count(r.r))
			IR.push_back(new Move(m2r[r.r], temp));
		else
			IR.push_back(new Store(temp, r.r, new var()));

		return retype(rd, etype::val, "int");
	}

	virtual antlrcpp::Any visitLeftselfaddsub(complierParser::LeftselfaddsubContext *ctx) override 
	{
		if (previsit)
		{
			side = true;
			return string("");
		}
		retype r = visit(ctx->expr());
		var *v = getval(r);
		comp.rel(v);
		var *temp = new var(comp.newreg());
		if (ctx->op->getText() == "++")
			IR.push_back(new Add(v, new var(1LL), temp));
		else if (ctx->op->getText() == "--")
			IR.push_back(new Sub(v, new var(1LL), temp));

		comp.rel(temp);
		if (m2r.count(r.r))
			IR.push_back(new Move(m2r[r.r], temp));
		else
			IR.push_back(new Store(temp, r.r, new var()));

		return retype(r.r, etype::addr, "int");
	}

	virtual antlrcpp::Any visitUcexpr(complierParser::UcexprContext *ctx) override 
	{
		if (ctx->expr() == nullptr)
			return retype(nullptr, etype::nop, "");
		return visit(ctx->expr());
	}

	virtual antlrcpp::Any visitWhilestat(complierParser::WhilestatContext *ctx) override 
	{
		var *bl = new var(comp.newlabel());
		var *el = new var(comp.newlabel());
		var *bodyl = new var(comp.newlabel());
		auto &fc = IR.getref();
		handle bh = fc.newblock(), eh = fc.newblock();
		handle body = fc.newblock();
		loopb.push_back(loop(bl, bh));
		loope.push_back(loop(el, eh));
		IR.push_back(new Jump(bl));
		fc.addto(bh);

		fc.chb(bh);
		IR.push_back(new lab(bl));
		retype r = visit(ctx->expr());
		var *v = getval(r);
		IR.push_back(new Branch(v, bodyl, el));
		comp.rel(v);
		fc.addto(body); fc.addto(eh);

		fc.chb(body);
		IR.push_back(new lab(bodyl));
		push();
		visit(ctx->block());
		pop();
		IR.push_back(new Jump(bl));
		fc.addto(bh);

		fc.chb(eh);
		IR.push_back(new lab(el));
		loopb.pop_back();
		loope.pop_back();


		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitIfstat(complierParser::IfstatContext *ctx) override 
	{
		var *tl = new var(comp.newlabel());
		var *fl = new var(comp.newlabel());
		var *el = new var(comp.newlabel());
		auto &fc = IR.getref();
		handle th = fc.newblock(), fh = fc.newblock(), eh = fc.newblock();
		retype r = visit(ctx->expr());
		var *v = getval(r);
		IR.push_back(new Branch(v, tl, fl));
		comp.rel(v);
		fc.addto(th); fc.addto(fh);

		fc.chb(th);
		IR.push_back(new lab(tl));
		push();
		visit(ctx->block(0));
		pop();
		IR.push_back(new Jump(el));
		fc.addto(eh);

		fc.chb(fh);
		IR.push_back(new lab(fl));
		if (ctx->ELSE())
		{
			push();
			visit(ctx->block(1));
			pop();
		}
		IR.push_back(new Jump(el));
		fc.addto(eh);

		fc.chb(eh);
		IR.push_back(new lab(el));

		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitForstat(complierParser::ForstatContext *ctx) override 
	{
		retype r0=visit(ctx->ucexpr(0));
		if (r0.r) comp.rel(r0.r);
		var *bl = new var(comp.newlabel());
		var *el = new var(comp.newlabel());
		var *body = new var(comp.newlabel());
		auto &fc = IR.getref();
		handle bh = fc.newblock(), eh = fc.newblock(), bodyh = fc.newblock();

		loopb.push_back(loop(bl,bh));
		loope.push_back(loop(el,eh));
		IR.push_back(new Jump(bl));
		fc.addto(bh);

		fc.chb(bh);
		IR.push_back(new lab(bl));
		retype r1 = visit(ctx->ucexpr(1));
		if (r1.r)
		{
			var *v = getval(r1);
			comp.rel(v);
			IR.push_back(new Branch(v, body, el));
			fc.addto(bodyh); fc.addto(eh);
		}
		else
		{
			IR.push_back(new Jump(body));
			fc.addto(bodyh);
		}

		fc.chb(bodyh);
		IR.push_back(new lab(body));
		push();
		visit(ctx->block());
		pop();

		retype r2=visit(ctx->ucexpr(2));
		if (r2.r) comp.rel(r2.r);
		IR.push_back(new Jump(bl));
		fc.addto(bh);

		fc.chb(eh);
		IR.push_back(new lab(el));
		loopb.pop_back();
		loope.pop_back();
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitBracket(complierParser::BracketContext *ctx) override 
	{
		return visit(ctx->expr());
	}

	virtual antlrcpp::Any visitNegative(complierParser::NegativeContext *ctx) override 
	{
		if (previsit)
		{
			visit(ctx->expr());
			return string("int");
		}
		retype r = visit(ctx->expr());
		var *rs = getval(r);
		comp.rel(rs);
		var *rd = new var(comp.newreg());
		IR.push_back(new Neg(rd, rs));

		return retype(rd, etype::val, r.tn);
	}

	virtual antlrcpp::Any visitNot(complierParser::NotContext *ctx) override 
	{
		if (previsit)
		{
			visit(ctx->expr());
			return string("bool");
		}
		retype r = visit(ctx->expr());
		var *rs = getval(r);
		comp.rel(rs);
		var *rd = new var(comp.newreg());
		IR.push_back(new Not(rd, rs));

		return retype(rd, etype::val, r.tn);
	}

	virtual antlrcpp::Any visitRev(complierParser::RevContext *ctx) override 
	{
		if (previsit)
		{
			visit(ctx->expr());
			return string("int");
		}
		retype r = visit(ctx->expr());
		var *rs = getval(r);
		comp.rel(rs);
		var *rd = new var(comp.newreg());
		IR.push_back(new Rev(rd, rs));

		return retype(rd, etype::val, r.tn);
	}

	virtual antlrcpp::Any visitMuldivmod(complierParser::MuldivmodContext *ctx) override 
	{
		if (previsit)
		{
			if (ctx->op->getText() != "*")
				side = true;
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("int");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		if (ctx->op->getText() == "*")
				IR.push_back(new Mult(rs1, rs2, rd));
		else if (ctx->op->getText() == "/")
				IR.push_back(new Div(rs1, rs2, rd));
		else if (ctx->op->getText() == "%")
			IR.push_back(new Mod(rs1, rs2, rd));

		return ret;
	}

	virtual antlrcpp::Any visitShift(complierParser::ShiftContext *ctx) override
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("int");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		if (ctx->op->getText() == "<<")
			IR.push_back(new Lshift(rs1, rs2, rd));
		else if (ctx->op->getText() == ">>")
			IR.push_back(new Rshift(rs1, rs2, rd));

		return ret;
	}

	virtual antlrcpp::Any visitPartial(complierParser::PartialContext *ctx) override 
	{
		if (previsit)
		{
			string peek = visit(ctx->expr(0));
			visit(ctx->expr(1));
			if (peek == "string") side = true;
			return string("bool");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		if (r1.tn != "string")
		{
			if (ctx->op->getText() == "<")
				IR.push_back(new cmp_LT(rs1, rs2, rd));
			else if (ctx->op->getText() == ">")
				IR.push_back(new cmp_GT(rs1, rs2, rd));
			else if (ctx->op->getText() == "<=")
				IR.push_back(new cmp_LE(rs1, rs2, rd));
			else if (ctx->op->getText() == ">=")
				IR.push_back(new cmp_GE(rs1, rs2, rd));
			
		}
		else
		{
			vector<var*> args;
			args.push_back(rs1);
			args.push_back(rs2);
			IR.push_back(new Call(func2l["func_string"+ctx->op->getText()], args));
			IR.push_back(new Move(rd, retreg));
		}

		
		return ret;
	}

	virtual antlrcpp::Any visitCompare(complierParser::CompareContext *ctx) override 
	{
		if (previsit)
		{
			string peek = visit(ctx->expr(0));
			visit(ctx->expr(1));
			if (peek == "string") side = true;
			return string("bool");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		if (r1.tn != "string")
		{
			if (ctx->op->getText() == "==")
				IR.push_back(new cmp_EQ(rs1, rs2, rd));
			else if (ctx->op->getText() == "!=")
				IR.push_back(new cmp_NE(rs1, rs2, rd));
		}
		else
		{
			vector<var*> args;
			args.push_back(rs1);
			args.push_back(rs2);
			IR.push_back(new Call(func2l["func_string" + ctx->op->getText()], args));
			IR.push_back(new Move(rd, retreg));
		}

		return ret;
	}

	virtual antlrcpp::Any visitAnd(complierParser::AndContext *ctx) override 
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("int");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		IR.push_back(new And(rs1, rs2, rd));

		comp.rel(rs1);
		comp.rel(rs2);
		return ret;
	}

	virtual antlrcpp::Any visitXor(complierParser::XorContext *ctx) override
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("int");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		IR.push_back(new Xor(rs1, rs2, rd));

		return ret;
	}

	virtual antlrcpp::Any visitOr(complierParser::OrContext *ctx) override
	{
		if (previsit)
		{
			visit(ctx->expr(0));
			visit(ctx->expr(1));
			return string("int");
		}
		retype r1 = visit(ctx->expr(0)), r2 = visit(ctx->expr(1));
		var *rs1 = getval(r1), *rs2 = getval(r2);
		comp.rel(rs1);
		comp.rel(rs2);
		var *rd = new var(comp.newreg());
		retype ret(rd, etype::val, r1.tn);
		IR.push_back(new Or(rs1, rs2, rd));

		return ret;
	}

	virtual antlrcpp::Any visitBreakstat(complierParser::BreakstatContext *ctx) override 
	{
		IR.push_back(new Jump(loope.back().first));
		auto &fc = IR.getref();
		handle tb = fc.newblock();
		fc.addto(loope.back().second);
		fc.chb(tb);
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitContinuestat(complierParser::ContinuestatContext *ctx) override 
	{
		IR.push_back(new Jump(loopb.back().first));
		auto &fc = IR.getref();
		handle tb = fc.newblock();
		fc.addto(loopb.back().second);
		fc.chb(tb);
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitReturstat(complierParser::ReturstatContext *ctx) override 
	{
		if (ctx->expr())
		{
			retype r = visit(ctx->expr());
			var *v = getval(r);
			IR.push_back(new Return(v));

			comp.rel(v);
		}
		else
			IR.push_back(new Return(new var()));
		return retype(nullptr, etype::nop, "");
	}

	virtual antlrcpp::Any visitCallfunc(complierParser::CallfuncContext *ctx) override 
	{
		if (previsit)
		{	
			side = true;
			return string("");
		}
		retype r = visit(ctx->expr());
		if (!func2l.count(r.tn))
			throw "here";
		var *func = func2l[r.tn];
		vector<var*> pargs;
		if (vthis) pargs.push_back(vthis);
		vthis = nullptr;
		if (ctx->exprlist())
			for (auto i : ctx->exprlist()->expr())
				pargs.push_back(getval(visit(i)));
		for (auto v : pargs)
			comp.rel(v);
		IR.push_back(new Call(func, pargs));

		string ftype = func2type[r.tn];
		var *rd = new var(comp.newreg());
		IR.push_back(new Move(rd, retreg));
		return retype(rd, etype::val, ftype);
	}

	void assist(var* rd, vector<var*> args)
	{
		if (args.size() == 0)
		{
			
			/*if (!classsize.count(typenew))
			{
				IR.push_back(new Move(rd, new var(0LL)));
				return;
			}*/
			assert(classsize.count(typenew));
			args.clear();
			var *v = new var(classsize[typenew]);
			comp.rel(v);
			args.push_back(v);
			//var *rd = new var(comp.newreg());
			IR.push_back(new Call(func2l["func_new"], args));
			IR.push_back(new Move(rd, retreg));
			string cfuncn = "func_" + typenew + "." + typenew;
			if (func2l.count(cfuncn))
			{
				args.clear();
				args.push_back(rd);
				IR.push_back(new Call(func2l[cfuncn], args));
			}
			return;
		}
		var *s = args.back();
		args.pop_back();

		vector<var*> relist;

		var *ts = new var(comp.newreg());
		var *tt = new var(comp.newreg());
		relist.push_back(ts);
		relist.push_back(tt);

		IR.push_back(new Mult(s, new var(8LL), ts));
		IR.push_back(new Add(ts, new var(8LL), tt));
		vector<var*> pargs;
		pargs.push_back(tt);
		IR.push_back(new Call(func2l["func_new"], pargs));

		var *adr = new var(comp.newreg());
		relist.push_back(adr);

		IR.push_back(new Move(adr, retreg));
		IR.push_back(new Add(adr, new var(8LL), rd));
		IR.push_back(new Store(s, adr, new var()));

		if (args.size() == 0 && !classsize.count(typenew))
			return;

		var *addr = new var(comp.newreg());
		var *temp = new var(comp.newreg());
		relist.push_back(addr);
		relist.push_back(temp);

		IR.push_back(new Move(addr,rd));

		
		var *gr = new var(comp.newreg());
		relist.push_back(gr);

		IR.push_back(new Move(gr, s));
		var *bl = new var(comp.newlabel());
		var *el = new var(comp.newlabel());
		var *bodyl = new var(comp.newlabel());
		auto& fc = IR.getref();
		handle bh = fc.newblock(), eh = fc.newblock(),bodyh=fc.newblock();
		IR.push_back(new Jump(bl));
		fc.addto(bh);

		fc.chb(bh);
		IR.push_back(new lab(bl));

		var *isend = new var(comp.newreg());
		relist.push_back(isend);

		IR.push_back(new cmp_GE(gr, new var(0LL), isend));
		IR.push_back(new Branch(isend, bodyl, el));
		fc.addto(bodyh); fc.addto(eh);

		fc.chb(bodyh);
		IR.push_back(new lab(bodyl));
		assist(temp, args);
		IR.push_back(new Store(temp, addr, new var()));

		var *t1 = new var(comp.newreg()), *t2 = new var(comp.newreg());
		relist.push_back(t1); relist.push_back(t2);

		IR.push_back(new Add(addr, new var(8), t1));
		IR.push_back(new Move(addr, t1));
		IR.push_back(new Sub(gr, new var(1), t2));
		IR.push_back(new Move(gr, t2));
		IR.push_back(new Jump(bl));
		fc.addto(bh);
		
		fc.chb(eh);
		IR.push_back(new lab(el));

		for (auto v : relist)
			comp.rel(v);
		return;
	}

	virtual antlrcpp::Any visitNew(complierParser::NewContext *ctx) override 
	{
		if (previsit)
		{
			side = true;
			return string("");
		}
		vector<var*> args;
		auto e = ctx->expr();
		string name;
		if (ctx->ID())
			name = ctx->ID()->getText();
		else name = ctx->RAWTYPE()->getText();
		typenew = name;
		string type = name;
		for (int c = 0; c < e.size(); c++)
			type += "[]";
		/*if (e.size())
		{
			{*/
		for (auto i = e.rbegin(); i != e.rend(); ++i)
			args.push_back(getval(visit(*i)));
		var *rd = new var(comp.newreg());
		assist(rd, args);
		for (auto v : args)
			comp.rel(v);
		return retype(rd, etype::val, type);
	}
	virtual antlrcpp::Any visitDelc_cons_func(complierParser::Delc_cons_funcContext *ctx) override 
	{
		string fname = "func_" + prefix + ctx->ID()->getText();
		//fname = "func_" + fname;
		IR.chf(fname);
		var *l = func2l[fname];
		auto& fc = IR.getref();
		fc.chb(fc.newblock());
		IR.push_back(new lab(l));

		isinfunc = true;
		m2r.clear();
		push();
		if (prefix.empty())
			throw "here";
		if (!prefix.empty())
		{
			string type = prefix;
			type.pop_back();
			var *t = new var(comp.newreg(1));
			
			name2type.back()["_vthis"] = type;
			name2reg.back()["_vthis"] = t;
			m2r[t] = new var(comp.newreg(1));
			IR.push_back(new delc("_vthis", t, m2r[t]));
			IR.push_back(new Load(m2r[t], t, new var()));
		}
		for (auto i : ctx->block())
			visit(i);
		pop();
		m2r.clear();
		isinfunc = false;
		IR.push_back(new Return(new var()));
		IR.chf("global");
		return retype(nullptr, etype::nop, "");
	}
};

/*enum opid {
	ADD, SUB, MULT, DIV, MOD, LSHIFT, RSHIFT, OR, AND, XOR, CMP_LT, CMP_LE, CMP_EQ, CMP_GE, CMP_GT, CMP_NE, NOT, NEG, REV
};


enum v_type { immnum, regi };

struct _rvtype
{

	v_type typ;
	int vid;
	long long value;
	_rvtype(v_type p = immnum, int pid = -1, long long pval = 0LL)
		:typ(p), vid(pid), value(pval)
	{}
	bool operator<(const _rvtype & o) const
	{
		if (typ != o.typ) return typ < o.typ;
		if (typ == immnum) return value < o.value;
		return vid < o.vid;
	}
	bool operator==(const _rvtype & o) const
	{
		if (typ != o.typ) return false;
		if (typ == regi) return vid == o.vid;
		return value == o.value;
	}
	bool operator!=(const _rvtype & o) const
	{
		return !((*this) == o);
	}
};

inline var* vtrans(const _rvtype & rv)
{
	if (rv.typ == immnum)
		return new var(rv.value);
	if (rv.typ == regi)
		return new var(new reg(rv.vid));
}

class exxp
{
public:
	_rvtype id1;
	_rvtype id2;
	opid op;
	exxp(const _rvtype & pid1, const _rvtype & pid2, opid pop)
		:id1(pid1), id2(pid2), op(pop)
	{
		if (op == ADD || op == MULT || op == OR || op == AND || op == XOR || op == CMP_EQ || op == CMP_NE)
			if (id1 < id2)
			{
				auto t = id1;
				id1 = id2;
				id2 = t;
			}
	}

	bool operator<(const exxp & o) const
	{
		if (id1 != o.id1) return id1 < o.id1;
		if (id2 != o.id2) return id2 < o.id2;
		return op < o.op;
	}

};

class IRreducer
{
public:
	map<string, bool> safefunc;



	class brain
	{
	public:
		//vector<map<int, _rvtype>> mem;
		vector<map<int, _rvtype>> rval;
		vector<map<exxp, _rvtype>> cal;
		brain()
		{
			//mem.push_back(map<int, _rvtype>());
			rval.push_back(map<int, _rvtype>());
			cal.push_back(map<exxp, _rvtype>());
		}
		void push()
		{
			//mem.push_back(map<int, _rvtype>(mem.back()));
			rval.push_back(map<int, _rvtype>(rval.back()));
			cal.push_back(map<exxp, _rvtype>(cal.back()));
		}
		void pop()
		{
			//mem.pop_back();
			rval.pop_back();
			cal.pop_back();
		}
		void clear()
		{
			//mem.clear();
			rval.clear();
			cal.clear();
		}
		vector<inst*> brainfuck()
		{
			auto& rref = rval.back();
			vector<inst*> ret;
			for (auto i : rref)
				ret.push_back(new Move(new var(new reg(i.first)), vtrans(i.second)));
			cal.back().clear();
			rref.clear();
			return ret;
		}
		_rvtype getrv(var* x)
		{
			map<int, _rvtype>& rref = rval.back();
			if (x->type == regist)
			{
				int id = x->r->id;
				if (rref.count(id))
					return rref[id];
				else
					return _rvtype(regi, id);
			}
			if (x->type == imm)
				return _rvtype(immnum, -1, x->val);
		}


	};
public:
	IRtype IR;
	IRtype nIR;

	brain naive;

	IRreducer(const IRgen & i)
		:IR(i.IR)
	{
		for (int i = 0; i < 18; i++)
			safefunc["l" + to_string(i)] = true;
	}

	void print(ostream & out)
	{
		for (auto f : IR.fcs)
		{
			out << f.first << ":" << endl;
			auto &bbs = f.second.bbs;
			for (int i = 0; i < bbs.size(); i++)
			{
				auto &bb = bbs[i];
				out << i << ":" << endl;
				for (auto &stmt : bb.stmt)
					out << stmt->content() << endl;
				out << "link to:" << endl;
				for (auto &l : bb.to)
					out << l << " ";
				out << endl << endl;
			}
			out << endl;
		}
	}
	long long calincom(long long x, long long y, opid o)
	{
		long long ret;
		switch (o)
		{
		case ADD:
			ret = x + y;
			break;
		case SUB:
			ret = x - y;
			break;
		case MULT:
			ret = x*y;
			break;
		case DIV:
			ret = x / y;
			break;
		case MOD:
			ret = x%y;
			break;
		case LSHIFT:
			ret = x << y;
			break;
		case RSHIFT:
			ret = x >> y;
			break;
		case OR:
			ret = x | y;
			break;
		case AND:
			ret = x&y;
			break;
		case XOR:
			ret = x^y;
			break;
		case CMP_LT:
			ret = x < y;
			break;
		case CMP_LE:
			ret = x <= y;
			break;
		case CMP_EQ:
			ret = x == y;
			break;
		case CMP_GE:
			ret = x >= y;
			break;
		case CMP_GT:
			ret = x > y;
			break;
		case CMP_NE:
			ret = x != y;
			break;
		case NOT:
			ret = !x;
			break;
		case NEG:
			ret = -x;
			break;
		case REV:
			ret = ~x;
			break;
		}
		return ret;
	}
	template<typename T>
	T* binstmt(T* ptr, opid o)
	{
		auto id1 = naive.getrv(ptr->rs1);
		auto id2 = naive.getrv(ptr->rs2);
		auto& rref = naive.rval.back();
		if (id1.typ == immnum && id2.typ == immnum)
		{
			rref[ptr->rd->r->id] = _rvtype(immnum, -1, calincom(id1.value, id2.value, o));
			return nullptr;
		}
		exxp t(id1, id2, o);
		auto& cref = naive.cal.back();
		if (cref.count(t))
		{
			rref[ptr->rd->r->id] = cref[t];
			return nullptr;
		}
		else
		{
			auto stmt = new T(*ptr);
			stmt->rs1 = vtrans(id1);
			stmt->rs2 = vtrans(id2);
			cref[t] = _rvtype(regi, ptr->rd->r->id);
			return stmt;
		}
	}

	template<typename T>
	T* unistmt(T* ptr, opid o)
	{
		auto id1 = naive.getrv(ptr->rs);
		auto id2 = _rvtype(regi);
		auto& rref = naive.rval.back();
		if (id1.typ == immnum)
		{
			rref[ptr->rd->r->id] = _rvtype(immnum, -1, calincom(id1.value, 0LL, o));
			return nullptr;
		}
		exxp t(id1, id2, o);
		auto& cref = naive.cal.back();
		if (cref.count(t))
		{
			rref[ptr->rd->r->id] = cref[t];
			return nullptr;
		}
		else
		{
			auto stmt = new T(*ptr);
			stmt->rs = vtrans(id1);
			cref[t] = _rvtype(regi, ptr->rd->r->id);
			return stmt;
		}
	}
	vector<inst*> reduce(const vector<inst*> bb)
	{
		vector<inst*> ret;
		for (auto stmt : bb)
		{
			if (Add *ptr = dynamic_cast<Add*>(stmt))
			{
				auto r = binstmt<Add>(ptr, ADD);
				if (r) ret.push_back(r);
				continue;
			}
			if (Sub *ptr = dynamic_cast<Sub*>(stmt))
			{
				auto r = binstmt<Sub>(ptr, SUB);
				if (r) ret.push_back(r);
				continue;
			}
			if (Mult *ptr = dynamic_cast<Mult*>(stmt))
			{
				auto r = binstmt<Mult>(ptr, MULT);
				if (r) ret.push_back(r);
				continue;
			}
			if (Div *ptr = dynamic_cast<Div*>(stmt))
			{
				auto r = binstmt<Div>(ptr, DIV);
				if (r) ret.push_back(r);
				continue;
			}
			if (Mod *ptr = dynamic_cast<Mod*>(stmt))
			{
				auto r = binstmt<Mod>(ptr, MOD);
				if (r) ret.push_back(r);
				continue;
			}
			if (Lshift *ptr = dynamic_cast<Lshift*>(stmt))
			{
				auto r = binstmt<Lshift>(ptr, LSHIFT);
				if (r) ret.push_back(r);
				continue;
			}
			if (Rshift *ptr = dynamic_cast<Rshift*>(stmt))
			{
				auto r = binstmt<Rshift>(ptr, RSHIFT);
				if (r) ret.push_back(r);
				continue;
			}
			if (Or *ptr = dynamic_cast<Or*>(stmt))
			{
				auto r = binstmt<Or>(ptr, OR);
				if (r) ret.push_back(r);
				continue;
			}
			if (And *ptr = dynamic_cast<And*>(stmt))
			{
				auto r = binstmt<And>(ptr, AND);
				if (r) ret.push_back(r);
				continue;
			}
			if (Xor *ptr = dynamic_cast<Xor*>(stmt))
			{
				auto r = binstmt<Xor>(ptr, XOR);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_EQ *ptr = dynamic_cast<cmp_EQ*>(stmt))
			{
				auto r = binstmt<cmp_EQ>(ptr, CMP_EQ);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_NE *ptr = dynamic_cast<cmp_NE*>(stmt))
			{
				auto r = binstmt<cmp_NE>(ptr, CMP_NE);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_GT *ptr = dynamic_cast<cmp_GT*>(stmt))
			{
				auto r = binstmt<cmp_GT>(ptr, CMP_GT);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_GE *ptr = dynamic_cast<cmp_GE*>(stmt))
			{
				auto r = binstmt<cmp_GE>(ptr, CMP_GE);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_LT *ptr = dynamic_cast<cmp_LT*>(stmt))
			{
				auto r = binstmt<cmp_LT>(ptr, CMP_LT);
				if (r) ret.push_back(r);
				continue;
			}
			if (cmp_LE *ptr = dynamic_cast<cmp_LE*>(stmt))
			{
				auto r = binstmt<cmp_LE>(ptr, CMP_LE);
				if (r) ret.push_back(r);
				continue;
			}
			if (Not *ptr = dynamic_cast<Not*>(stmt))
			{
				auto r = unistmt<Not>(ptr, NOT);
				if (r) ret.push_back(r);
				continue;
			}
			if (Neg *ptr = dynamic_cast<Neg*>(stmt))
			{
				auto r = unistmt<Neg>(ptr, NEG);
				if (r) ret.push_back(r);
				continue;
			}
			if (Rev *ptr = dynamic_cast<Rev*>(stmt))
			{
				auto r = unistmt<Rev>(ptr, REV);
				if (r) ret.push_back(r);
				continue;
			}
			if (Move *ptr = dynamic_cast<Move*>(stmt))
			{
				_rvtype src = naive.getrv(ptr->rs);
				if (src.typ == regi && src.vid == 0)
				{
					ret.push_back(ptr);
					continue;
				}
				naive.rval.back()[ptr->rd->r->id] = src;
				continue;
			}
			if (Load *ptr = dynamic_cast<Load*>(stmt))
			{
				assert(ptr->rs2->type == nop);
				_rvtype src = naive.getrv(ptr->rs1);
				Load *temp = new Load(*ptr);
				temp->rs1 = vtrans(src);
				ret.push_back(temp);
				continue;
			}
			if (Store *ptr = dynamic_cast<Store*>(stmt))
			{
				assert(ptr->rd2->type == nop);
				_rvtype des = naive.getrv(ptr->rd1);
				_rvtype src = naive.getrv(ptr->rs);
				Store *temp = new Store(*ptr);
				temp->rs = vtrans(src);
				temp->rd1 = vtrans(des);
				ret.push_back(temp);
				continue;
			}
			if (Call *ptr = dynamic_cast<Call*>(stmt))
			{
				vector<var*> pargs;
				for (auto v : ptr->args)
					pargs.push_back(vtrans(naive.getrv(v)));
				if (ptr->func->l != "malloc" && !safefunc.count(ptr->func->l))
				{
					auto r = naive.brainfuck();
					for (auto s : r)
						ret.push_back(s);
				}
				ret.push_back(new Call(ptr->func, pargs));
				continue;
			}
			if (Branch *ptr = dynamic_cast<Branch*>(stmt))
			{
				ret.push_back(new Branch(vtrans(naive.getrv(ptr->rsc)), ptr->rs1, ptr->rs2));
				continue;
			}
			if (Return *ptr = dynamic_cast<Return*>(stmt))
			{
				ret.push_back(new Return(vtrans(naive.getrv(ptr->rs))));
				continue;
			}
			ret.push_back(stmt);
		}
		return ret;
	}

	void viormv()
	{
		for (auto fc : IR.fcs)
		{
			auto bbs = fc.second.bbs;
			vector<basicblock> nbbs;
			for (auto bb : bbs)
			{
				basicblock nbb;
				nbb.to = bb.to;
				naive.push();
				nbb.stmt = reduce(bb.stmt);
				naive.pop();
				nbbs.push_back(nbb);
			}
			nIR.fcs[fc.first].bbs = nbbs;
		}
		IR = nIR;
	}
	void dfs(int now, vector<basicblock>& bbs, int* v, int* in, queue<int>& q)
	{
		naive.push();
		auto nstmt = reduce(bbs[now].stmt);
		bbs[now].stmt = nstmt;
		bool fucked = false;
		for (auto to : bbs[now].to)
			if (!v[to])
			{
				if (in[to] == 1)
				{
					v[to] = 1;
					dfs(to, bbs, v, in, q);
				}
				else
				{
					fucked = true;
					q.push(to);
				}
			}
		if (fucked)
		{
			auto r = naive.brainfuck();
			auto& st = bbs[now].stmt;
			auto temp = st.back();
			st.pop_back();
			for (auto s : r)
				st.push_back(s);
			st.push_back(temp);
		}
		naive.pop();
	}
	void smartrmv()
	{
		for (auto fc : IR.fcs)
		{
			vector<basicblock> nbbs = fc.second.bbs;
			int sz = nbbs.size();
			int *v = new int[sz];
			int *in = new int[sz];
			for (int i = 0; i < sz; i++)
				v[i] = in[i] = 0;
			for (int i = 0; i < sz; i++)
				for (auto to : nbbs[i].to)
					in[to]++;
			v[0] = 1;
			queue<int> q;
			q.push(0);
			while (!q.empty())
			{
				int now = q.front();
				q.pop();
				dfs(now, nbbs, v, in, q);
			}
			nIR.fcs[fc.first].bbs = nbbs;
		}
		IR = nIR;
	}
};
*/