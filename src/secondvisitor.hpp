#pragma once
#include"firstvisitor.hpp"
#include<complierBaseVisitor.h>
#include<unordered_map>
#include<vector>
using namespace std;

class secondvisitor : public complierBaseVisitor
{
public:
	bool result;
	int inloop;
	typedef unordered_map < string, string > table;
	vector<table> label;
	unordered_map<string, functype> funcs;
	string thistype;
	string funcretype;
	struct exprtype
	{
		string type;
		bool isleft;
		bool isfunc;
		bool isarray;
		functype ft;
		exprtype(string p_type=string(""),bool p_isleft=false,bool p_isfunc=false,bool p_isarray=false,functype p_ft=functype())
			:type(p_type),isleft(p_isleft),isfunc(p_isfunc),isarray(p_isarray),ft(p_ft){}
	};
	
	string find(const string & sym)
	{
		for (auto iter = label.crbegin(); iter != label.crend(); ++iter)
		{
			auto temp = iter->find(sym);
			if (temp != iter->end())
				return temp->second;
		}
		return string("");
	}
	exprtype gettype(const string & name)
	{
		exprtype ret;
		string type;
		string realname = name;
		auto temp = label.crbegin()->find(realname);
		type = (temp == label.crbegin()->end() ? "" : temp->second);
		if (type.empty() && thistype.size())
			/*if (thistype.size() && thistype == realname)
				type = find(realname);
			else*/
				realname = thistype + "." + name, type = find(thistype + "." + name);
		if (type.empty()) realname = name, type = find(name);
		ret.type = type;
		ret.isarray = is_arr(type);
		if (type.empty())
			cerr << "error:not declared\n" << name << endl;
		else if (type == "func")
		{
			ret.isfunc = true;
			ret.ft = funcs[realname];
		}
		else ret.isleft = (type!="class");
		return ret;
	}
	string typefind(const string & type)
	{

	}
	exprtype totype(const string & rua)
	{
		exprtype ret;
		ret.type = rua;
		ret.isarray = is_arr(rua);
		string type = find(rua);
		if (type == "func")
		{
			ret.isfunc = true;
			ret.ft = funcs[rua];
		}
		return ret;
	}
	bool is_arr(const string & type)
	{
		return type.size() && type.back() == ']';
	}
	bool is_equal(const exprtype & a, const exprtype & b)
	{
		if (a.isfunc || b.isfunc) return false;
		if (a.type == b.type) return true;
		if (b.type == "null")
			return is_arr(a.type) || find(a.type) == "class";
		return false;
	}
public:
	secondvisitor(const firstvisitor & fv) :funcs(fv.funcs)
	{
		table gl(fv.name2type);
		label.push_back(std::move(gl));
		thistype = "";
		result = fv.result;
		inloop = 0;
	}
	bool isvalid()
	{
		if (!result) return false;
		if (!funcs.count("main")) return false;
		if (funcs["main"].retype != "int") return false;
		return true;
	}
	virtual antlrcpp::Any visitAddsub(complierParser::AddsubContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type =="int" &&  type2.type == "int")
			return exprtype("int");
		if (type1.type == "string" && type2.type == "string")
			return exprtype("string");
		cerr << "error:type cannot plus\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitNum(complierParser::NumContext *ctx) override 
	{
		return exprtype("int");
	}
	virtual antlrcpp::Any visitTrue(complierParser::TrueContext *ctx) override 
	{
		return exprtype("bool");
	}
	virtual antlrcpp::Any visitFalse(complierParser::FalseContext *ctx) override 
	{
		return exprtype("bool");
	}
	virtual antlrcpp::Any visitNull(complierParser::NullContext *ctx) override 
	{
		return exprtype("null");
	}
	virtual antlrcpp::Any visitConststr(complierParser::ConststrContext *ctx) override 
	{
		return exprtype("string");
	}
	virtual antlrcpp::Any visitId(complierParser::IdContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		return gettype(name);
	}
	virtual antlrcpp::Any visitDelc_var(complierParser::Delc_varContext *ctx) override 
	{
		exprtype vtype = visit(ctx->type());
		if (vtype.type == "" || vtype.type == "void")
		{
			cerr << "error:not a type\n" << ctx->getText() << endl;
			result = false;
			return exprtype();
		}
		string name = ctx->ID()->getText();
		if (label.crbegin()->count(name))
			cerr << "error:has declared\n" << ctx->getText() << endl, result = false;
		else
			(*label.rbegin())[name] = ctx->type()->getText();
		if (ctx->expr() != nullptr)
		{
			exprtype etype = visit(ctx->expr());
			if (!is_equal(vtype, etype))
				cerr << "error:not same type\n" << ctx->getText() << endl, result = false;
		}
		return exprtype();
	}
	virtual antlrcpp::Any visitNewblock(complierParser::NewblockContext *ctx) override 
	{
		label.push_back(move(table()));
		visitChildren(ctx);
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitAssign(complierParser::AssignContext *ctx) override
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (!type1.isleft) cerr << "error:not left\n" << ctx->getText() << endl, result = false;
		if (!is_equal(type1, type2))
			cerr << "error:not same type\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitCallfunc(complierParser::CallfuncContext *ctx) override 
	{
		exprtype func = visit(ctx->expr());
		if (!func.isfunc)
		{
			cerr << "error:not func\n" << ctx->getText() << endl;
			result = false;
			return exprtype();
		}
		int sz = 0;
		if ((sz = func.ft.paratype.size()) != (ctx->exprlist()==nullptr?0:ctx->exprlist()->expr().size()))
		{
			cerr << "error:para number not same\n" << ctx->getText() << endl;
			result = false;
			return exprtype();
		}
		for (int i = 0; i < sz; i++)
		{
			exprtype temp = visit(ctx->exprlist()->expr(i));
			if (!is_equal(totype(func.ft.paratype[i]), temp))
			{
				cerr << "error:not same type\n" << ctx->exprlist()->expr(i)->getText() << endl;
				result = false;
				return exprtype();
			}
		}
		return exprtype(func.ft.retype,false,false,is_arr(func.ft.retype));
	}
	virtual antlrcpp::Any visitMember(complierParser::MemberContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr());
		bool isclass = label.begin()->count(type1.type) && (*label.begin())[type1.type] == "class";
		if (isclass || type1.type == "string")
			return gettype(type1.type + '.' + ctx->ID()->getText());
		else if (type1.isarray)
			return gettype("array." + ctx->ID()->getText());
		else
		{
			cerr << "error:not a class\n" << ctx->getText() << endl;
			result = false;
			return exprtype();
		}
		
	}
	virtual antlrcpp::Any visitDelcfunc(complierParser::DelcfuncContext *ctx) override 
	{
		exprtype ftype = visit(ctx->type());
		if (ftype.type == "")
		{
			cerr << "error:not a type\n" << ctx->getText() << endl;
			result = false;
			return exprtype();
		}
		table temp;
		for (auto i : ctx->delc_only())
		{
			exprtype t = visit(i->type());
			string name = i->ID()->getText();
			string f = t.type;
			while (f.size() && (f.back() == ']' || f.back() == '[')) f.pop_back();
			if (f == "" || f == "void")
			{
				cerr << "error:not a type\n" << name << endl;
				result = false;
				continue;
			}
			if (temp.count(name))
			{
				cerr << "error:has been declared\n" << name << endl;
				result = false;
				continue;
			}
			temp[name] = t.type;
		}
		label.push_back(move(temp));
		funcretype = ctx->type()->getText();
		for (auto i : ctx->block())
			visit(i);
		funcretype = "";
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitClassbody(complierParser::ClassbodyContext *ctx) override 
	{
		label.push_back(move(table()));
		visitChildren(ctx);
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitType(complierParser::TypeContext *ctx) override 
	{
		//bool isarray = (ctx->getText().back() == ']');
		if (ctx->RAWTYPE() != nullptr)
			return exprtype(ctx->getText());
		string name = ctx->ID()->getText();
		exprtype t = gettype(name);
		if (t.type == "class")
			return exprtype(ctx->getText());
		return exprtype();
	}
	virtual antlrcpp::Any visitThis(complierParser::ThisContext *ctx) override 
	{
		return exprtype(thistype);
	}
	virtual antlrcpp::Any visitDelcclass(complierParser::DelcclassContext *ctx) override 
	{
		thistype = ctx->ID()->getText();
		visit(ctx->classbody());
		thistype = "";
		return exprtype();
	}
	virtual antlrcpp::Any visitDelc_only(complierParser::Delc_onlyContext *ctx) override 
	{
		exprtype vtype = visit(ctx->type());
		if (vtype.type == "" || vtype.type == "void")
			cerr << "error:not a type\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitIndex(complierParser::IndexContext *ctx) override 
	{
		exprtype itype = visit(ctx->expr(1));
		if (itype.type != "int")
		{
			cerr << "error:index not a num\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		exprtype ltype = visit(ctx->expr(0));
		string type = ltype.type;
		bool flag = true;
		if (flag &= (!type.empty() && type.back() == ']')) type.pop_back();
		if (flag &= (!type.empty() && type.back() == '[')) type.pop_back();
		if (!flag)
		{
			cerr << "error:not an array\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype(type, ltype.isleft,false,is_arr(type));
	}
	virtual antlrcpp::Any visitNew(complierParser::NewContext *ctx) override 
	{
		for (auto i : ctx->expr())
		{
			exprtype temp = visit(i);
			if (temp.type != "int")
			{
				cerr << "not a num\n" << i->getText() << endl, result = false;
				return exprtype();
			}
		}
		int p = -1, q = -1;
		for (auto i : ctx->children)
		{
			++p;
			if (antlrcpp::is<complierParser::ExprContext*>(i))
			{
				if (q == -1)
				{
					if (p != 3)
					{
						cerr << "error:new rua\n" << ctx->getText() << endl, result = true;
						return exprtype();
					}
					q = p;
					continue;
				}
				else if (p == q + 3)
				{
					q = p;
					continue;
				}
				else
				{
					cerr << "error:new rua\n" << ctx->getText() << endl, result = false;
					return exprtype();
				}
			}
		}
		string rtype;
		if (ctx->RAWTYPE() != nullptr)
			rtype = ctx->RAWTYPE()->getText();
		else
		{
			string name = ctx->ID()->getText();
			exprtype t = gettype(name);
			if (t.type == "class")
				rtype = name;
			else
			{
				cerr << "not a type\n" << ctx->getText() << endl, result = false;
				return exprtype();
			}
		}
		for (int i = 0; i < ctx->LeftF().size(); i++)
			rtype += "[]";
		return exprtype(rtype, true, false, q == -1);
	
	}
	virtual antlrcpp::Any visitReturstat(complierParser::ReturstatContext *ctx) override 
	{
		if (ctx->expr())
		{
			exprtype type = visit(ctx->expr());
			if (!is_equal(totype(funcretype), type))
				cerr << "error:return type not same\n" << ctx->getText()<<endl, result = false;
		}
		else
			if(funcretype!="void")
				cerr << "error:return type not same\n" << ctx->getText()<<endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitBracket(complierParser::BracketContext *ctx) override 
	{
		return visit(ctx->expr());
	}
	virtual antlrcpp::Any visitRightselfaddsub(complierParser::RightselfaddsubContext *ctx) override 
	{
		exprtype ltype = visit(ctx->expr());
		if (!ltype.isleft)
		{
			cerr << "error:not a left\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		if (ltype.type != "int")
		{
			cerr << "error:not a num\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype("int");
	}
	virtual antlrcpp::Any visitNegative(complierParser::NegativeContext *ctx) override 
	{
		exprtype ltype = visit(ctx->expr());
		if (ltype.type != "int")
		{
			cerr << "error:not a num\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype("int");
	}
	virtual antlrcpp::Any visitLeftselfaddsub(complierParser::LeftselfaddsubContext *ctx) override 
	{
		exprtype ltype = visit(ctx->expr());
		if (!ltype.isleft)
		{
			cerr << "error:not a left\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		if (ltype.type != "int")
		{
			cerr << "error:not a num\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype("int",true);
	}
	virtual antlrcpp::Any visitNot(complierParser::NotContext *ctx) override 
	{
		exprtype ltype = visit(ctx->expr());
		if (ltype.type != "bool")
		{
			cerr << "error:not a bool\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype("bool");
	}
	virtual antlrcpp::Any visitRev(complierParser::RevContext *ctx) override 
	{
		exprtype ltype = visit(ctx->expr());
		if (ltype.type != "int")
		{
			cerr << "error:not a num\n" << ctx->getText() << endl, result = false;
			return exprtype();
		}
		return exprtype("int");
	}
	virtual antlrcpp::Any visitMuldivmod(complierParser::MuldivmodContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "int" && type2.type == "int")
			return exprtype("int");
		else cerr << "error:not int\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitShift(complierParser::ShiftContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "int" && type2.type == "int")
			return exprtype("int");
		else cerr << "error:not int\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitPartial(complierParser::PartialContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if ((type1.type == "int" && type2.type == "int") || (type1.type == "string" && type2.type == "string"))
			return exprtype("bool");
		else cerr << "error:cannot compare\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitCompare(complierParser::CompareContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (is_equal(type1, type2) || is_equal(type2, type1))
			return exprtype("bool");
		else cerr << "error:cannot compare\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitAnd(complierParser::AndContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "int" && type2.type == "int")
			return exprtype("int");
		return exprtype();
	}
	virtual antlrcpp::Any visitXor(complierParser::XorContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "int" && type2.type == "int")
			return exprtype("int");
		else cerr << "error:not int\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitOr(complierParser::OrContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "int" && type2.type == "int")
			return exprtype("int");
		else cerr << "error:not int\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitLogicand(complierParser::LogicandContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "bool" && type2.type == "bool")
			return exprtype("bool");
		else cerr << "error:not bool\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitLogicor(complierParser::LogicorContext *ctx) override 
	{
		exprtype type1 = visit(ctx->expr(0));
		exprtype type2 = visit(ctx->expr(1));
		if (type1.type == "bool" && type2.type == "bool")
			return exprtype("bool");
		else cerr << "error:not bool\n" << ctx->getText() << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitIfstat(complierParser::IfstatContext *ctx) override 
	{
		exprtype etype = visit(ctx->expr());
		if (etype.type != "bool")
			cerr << "error:not a bool\n" << ctx->getText() << endl, result = false;
		for (auto i : ctx->block())
		{
			label.push_back(move(table()));
			visit(i);
			label.pop_back();
		}
		return exprtype();
	}
	virtual antlrcpp::Any visitWhilestat(complierParser::WhilestatContext *ctx) override 
	{
		exprtype etype = visit(ctx->expr());
		if (etype.type != "bool")
			cerr << "error:not a bool\n" << ctx->getText() << endl, result = false;
		label.push_back(move(table()));
		++inloop;
		visit(ctx->block());
		--inloop;
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitForstat(complierParser::ForstatContext *ctx) override 
	{
		if (ctx->ucexpr(0)->expr()) visit(ctx->ucexpr(0)->expr());
		if (ctx->ucexpr(2)->expr()) visit(ctx->ucexpr(2)->expr());
		if (ctx->ucexpr(1)->expr() != nullptr)
		{
			exprtype etype = visit(ctx->ucexpr(1)->expr());
			if (etype.type != "bool")
				cerr << "error:not a bool\n" << ctx->getText() << endl, result = false;
		}
		label.push_back(move(table()));
		++inloop;
		visit(ctx->block());
		--inloop;
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitDelc_cons_func(complierParser::Delc_cons_funcContext *ctx) override 
	{
		label.push_back(move(table()));
		funcretype = "void";
		for (auto i : ctx->block())
			visit(i);
		funcretype = "";
		label.pop_back();
		return exprtype();
	}
	virtual antlrcpp::Any visitEmptystat(complierParser::EmptystatContext *ctx) override 
	{
		return exprtype();
	}

	virtual antlrcpp::Any visitBreakstat(complierParser::BreakstatContext *ctx) override 
	{
		if (!inloop)
			cerr << "error:not in loop\n" << endl, result = false;
		return exprtype();
	}
	virtual antlrcpp::Any visitContinuestat(complierParser::ContinuestatContext *ctx) override 
	{
		if (!inloop)
			cerr << "error:not in loop\n" << endl, result = false;
		return exprtype();
	}
};