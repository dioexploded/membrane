#include "generator.hpp"

const string viogen::sixreg[6] = { "rdi","rsi","rdx","rcx","r8","r9" };
const string viogen::regs[16] = { "rax","rcx","rdx","rbx","rsp","rbp","rsi","rdi","r8","r9","r10","r11","r12","r13","r14","r15" };

const string smartgen::sixreg[6] = { "rdi","rsi","rdx","rcx","r8","r9" };
const string smartgen::regs[16] = { "rax","rcx","rdx","rbx","rsp","rbp","rsi","rdi","r8","r9","r10","r11","r12","r13","r14","r15" };
const string smartgen::regsb[16] = { "al","cl","dl","bl","spl","bpl","sil","dil","r8b","r9b","r10b","r11b","r12b","r13b","r14b","r15b" };
const vector<int> smartgen::order = { 15,14,13,12,11,10,9,8,7,6,3,2,1,0};
const vector<int> smartgen::sixid = { 7,6,2,1,8,9 };
const vector<int> smartgen::notsixid = { 15,14,13,12,11,10,3,0 };