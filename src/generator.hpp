#pragma once
#include"IR.hpp"
#include<fstream>
#include<string>
#include<iostream>
#include<sstream>
using namespace std;
class viogen
{
public:
	unordered_map<string, vector<inst*>> st;
	unordered_map<string, int> func2n;
	unordered_map<string, int> rspoff;

	vector<string> cstr;
	vector<int> cstr2reg;

	struct cons
	{
		int voff;
		string l;
		cons(int poff, const string & pl)
			:voff(poff), l(pl)
		{}
	};
	class regd
	{
	public:
		int off;
		cons *c;
		bool f;
		regd(int poff = 0, cons *pc = nullptr, bool pf = false)
			:off(poff), f(pf), c(pc)
		{}
	};
	map<int, cons*> bfreg;
	map<int, int> reg2off;
	map<int, cons*> freg;
	int offset;	
	static const string sixreg[6];
	static const string regs[16];
	ofstream& out;
	viogen(const IRgen & IR, ofstream& pout,const IRtype & IRcode)
		:func2n(IR.func2n), cstr(IR.cstr), cstr2reg(IR.cstr2reg), out(pout), offset(0)
	{
		for (auto fc : IRcode.fcs)
		{
			string fname = fc.first;
			vector<inst*> temp;
			auto& bbs = fc.second.bbs;
			int sz = bbs.size();
			int *v = new int[sz];
			int *stack = new int[sz];
			int top = 0;
			for (int i = 0; i < sz; i++) v[i] = 0;
			stack[top++] = 0;
			v[0] = 1;
			while (top)
			{
				int index = stack[--top];
				auto& bb = bbs[index];
				for (auto stmt : bb.stmt) temp.push_back(stmt);
				for (auto next : bb.to)
					if (!v[next])
					{
						v[next] = 1;
						stack[top++] = next;
					}
			}
			st[fname] = temp;
		}
	}
	regd getoff(int x)
	{
		if (reg2off.count(x)) return regd(reg2off[x],nullptr,false);
		if (freg.count(x)) return regd(0, freg[x], true);

		throw "there";
		return 0;
	}
	void m2reg(int id,int off)
	{
		out << "mov " << regs[id] << ",[rbp-" << off << ']' << endl;
	}
	void reg2m(int id, int off)
	{
		out << "mov " << "[rbp-" << off << "]," << regs[id] << endl;
	}
	bool var2reg(int id, var *v)
	{
		if (v->type == vartype::nul)
			return false;
		if (v->type == vartype::imm)
		{
			out << "mov " << regs[id] << "," << v->val << endl;
			return true;
		}
		if (v->type == vartype::regist)
		{
			if (v->r->id == 0)
			{
				out << "mov " << regs[id] << "," << regs[0] << endl;
				return true;
			}
			regd d = getoff(v->r->id);
			if (d.f)
			{
				if (d.c->voff)
				{
					out << "mov " << regs[id] << ",rbp" << endl;
					out << "sub " << regs[id] << "," << d.c->voff << endl;
				}
				else
					out << "mov " << regs[id] << "," << d.c->l << endl;
			}
			else
				m2reg(id, d.off);
			return true;
		}
		return true;
	}
	void givepos(var *v)
	{
		if (v->type != vartype::regist) return;
		int id = v->r->id;
		if (reg2off.count(id) || freg.count(id)) return;
		offset += 8;
		reg2off[id] = offset;
		return;
	}
	vector<int> strans(string x)
	{
		vector<int> r;
		r.push_back(0);
		int l = 0;
		for (int i = 0; i < x.length(); i++)
		{
			if (x[i] != '\\') r.push_back(x[i]);
			else
			{
				++i;
				if (x[i] == 'n') r.push_back(10);
				if (x[i] == '\\') r.push_back(92);
				if (x[i] == 't') r.push_back(9);
				if (x[i] == '\"') r.push_back(34);
			}
			++l;
		}
		r.front() = l;
		return r;
	}
	void prework()
	{
		ifstream h("head.txt");
		string temp;
		while (h.good())
		{
			getline(h, temp);
			out << temp << endl;
		}
		h.close();
		auto g = st["global"];
		{
			int id = 0;
			auto gl = st["global"];
			out << "section .bss" << endl;
			for (auto stmt : gl)
			{
				if (delc *ptr = dynamic_cast<delc*>(stmt))
				{
					out << "gv" << id << ":" << endl;
					out << "resb 8" << endl;
					bfreg[ptr->r->r->id] = new cons(0, "gv" + to_string(id));
					++id;
				}
			}
			out << endl;
		}
		
		out << "section .data" << endl;
		for (int i = 0; i < cstr.size(); i++)
		{
			vector<int> r = strans(cstr[i]);
			out << "dq " << r.front() << endl;
			out << "_cstr" << i << ":" << endl << "db ";
			for (int i = 1; i < r.size(); i++)
				out << r[i] << ",";
			out << "0";
			out << endl << endl;
			bfreg[cstr2reg[i]] = new cons(0, "_cstr" + to_string(i));
		}
	}

	void prefunc(const pair<const string, vector<inst*>>& func)
	{
		reg2off.clear();
		freg = bfreg;
		vector<var*> worklist;
		bool isglobal = false;
		if (func.first == "global") isglobal = true;
		offset = 0;
		for (auto stmt : func.second)
		{
			if (delc *ptr = dynamic_cast<delc*>(stmt))
			{
				if (isglobal) continue;
				//offset += 8;
				//if (ptr->v) worklist.push_back(ptr->v);
				if (ptr->v) givepos(ptr->v);
				freg[ptr->r->r->id] = new cons(offset, string());
				continue;
			}
			if (bincalc *ptr = dynamic_cast<bincalc*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Not *ptr = dynamic_cast<Not*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Neg *ptr = dynamic_cast<Neg*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Rev *ptr = dynamic_cast<Rev*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Load *ptr = dynamic_cast<Load*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Move *ptr = dynamic_cast<Move*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
		}
		for (auto v : worklist)
			givepos(v);
		offset -= 8 * func2n[func.first];
		if (offset % 16 != 0) offset += 8;
		rspoff[func.first] = offset;
	}
	void trans()
	{
		
		prework();
		for (auto func : st)
		{
			prefunc(func);
			out << "section .text" << endl;
			out << ";" + func.first << endl;
			auto stmt = func.second.begin();
				out << (*stmt)->content() << endl;
			++stmt;
			out << "push rbp" << endl;
			out << "mov rbp,rsp" << endl;
			int n = func2n[func.first];
			for (int i = 0; i < min(n, 6); i++)
				out << "push " + sixreg[i] << endl;
			if (n > 6)
				for (int i = 0; i < n - 6; i++)
					out << "push qword[rbp+" << (n - 5 - i) * 8 << ']' << endl;
			out << "sub rsp," << rspoff[func.first] << endl;
			for (; stmt != func.second.end(); ++stmt)
			{

				if (lab *ptr = dynamic_cast<lab*>(*stmt))
				{
					out << ptr->content() << endl;
					continue;
				}
				if (bincalc *ptr = dynamic_cast<bincalc*>(*stmt))
				{
					var2reg(12, ptr->rs1);
					var2reg(13, ptr->rs2);
					regd d = getoff(ptr->rd->r->id);
					if (cmp_base* cmp = dynamic_cast<cmp_base*>(ptr))
					{
						
						out << "cmp " << regs[12] << "," << regs[13] << endl;
						out << "mov " << regs[15] << ",0" << endl;
						if (dynamic_cast<cmp_EQ*>(cmp))
							out << "sete ";
						else if (dynamic_cast<cmp_GE*>(cmp))
							out << "setge ";
						else if (dynamic_cast<cmp_GT*>(cmp))
							out << "setg ";
						else if (dynamic_cast<cmp_LE*>(cmp))
							out << "setle ";
						else if (dynamic_cast<cmp_LT*>(cmp))
							out << "setl ";
						else if (dynamic_cast<cmp_NE*>(cmp))
							out << "setne ";
						
						out << regs[15]+"B" << endl;
						reg2m(15, d.off);
					}
					else
					{
						if (dynamic_cast<Add*>(ptr))
							out << "add ";
						else if (dynamic_cast<Sub*>(ptr))
							out << "sub ";
						else if (dynamic_cast<Mult*>(ptr))
							out << "imul ";
						else if (dynamic_cast<Div*>(ptr))
						{
							out << "mov rax," << regs[12] << endl;
							out << "cqo" << endl;
							out << "idiv " << regs[13] << endl;
							reg2m(0, d.off);
							continue;
						}
						else if (dynamic_cast<Mod*>(ptr))
						{
							out << "mov rax," << regs[12] << endl;
							out << "cqo" << endl;
							out << "idiv " << regs[13] << endl;
							reg2m(2, d.off);
							continue;
						}
						else if (dynamic_cast<Lshift*>(ptr))
						{
							out << "mov rcx," << regs[13] << endl;
							out << "sal " << regs[12] << ",cl" << endl;
							reg2m(12, d.off);
							continue;
						}
						else if (dynamic_cast<Rshift*>(ptr))
						{
							out << "mov rcx," << regs[13] << endl;
							out << "sar " << regs[12] << ",cl" << endl;
							reg2m(12, d.off);
							continue;
						}
						else if (dynamic_cast<Or*>(ptr))
							out << "or ";
						else if (dynamic_cast<And*>(ptr))
							out << "and ";
						else if (dynamic_cast<Xor*>(ptr))
							out << "xor ";
						out << regs[12] << "," << regs[13] << endl;
						reg2m(12, d.off);
						continue;
					}
				}
				if (Not *ptr = dynamic_cast<Not*>(*stmt))
				{
					var2reg(12, ptr->rs);
					regd d = getoff(ptr->rd->r->id);
					out << "xor " << regs[12] << ",1" << endl;
					reg2m(12, d.off);
					continue;
				}
				if (Neg *ptr = dynamic_cast<Neg*>(*stmt))
				{
					var2reg(12, ptr->rs);
					regd d = getoff(ptr->rd->r->id);
					out << "neg " << regs[12] << endl;
					reg2m(12, d.off);
					continue;
				}
				if (Rev *ptr = dynamic_cast<Rev*>(*stmt))
				{
					var2reg(12, ptr->rs);
					regd d = getoff(ptr->rd->r->id);
					out << "not " << regs[12] << endl;
					reg2m(12, d.off);
					continue;
				}
				if (Load *ptr = dynamic_cast<Load*>(*stmt))
				{
					var2reg(12, ptr->rs1);
					if(var2reg(13,ptr->rs2))
						out << "mov " << regs[12] << ",[" << regs[13] << "+" << regs[12] << "]" << endl;
					else out << "mov " << regs[12] << ",[" << regs[12] << "]" << endl;
					regd d = getoff(ptr->rd->r->id);
					out << "mov [rbp-" << d.off << "]," << regs[12] << endl;
					continue;
				}
				if (Store *ptr = dynamic_cast<Store*>(*stmt))
				{
					var2reg(13, ptr->rd1);
					if(var2reg(12,ptr->rd2))
						out << "add " << regs[13] << "," << regs[12] << endl;
					var2reg(12, ptr->rs);
					out << "mov [" << regs[13] << "]," << regs[12] << endl;
					continue;
				}
				if (Move *ptr = dynamic_cast<Move*>(*stmt))
				{
					var2reg(12, ptr->rs);
					regd d = getoff(ptr->rd->r->id);
					out << "mov [rbp-" << d.off << "]," << regs[12] << endl;
					continue;
				}
				if (Jump *ptr = dynamic_cast<Jump*>(*stmt))
				{
					out << "jmp " << ptr->rs->l << endl;
					continue;
				}
				if (Branch *ptr = dynamic_cast<Branch*>(*stmt))
				{
					bool f = false;
					var2reg(12, ptr->rsc);
					if (ptr->rs1->type != vartype::nul)
					{
						out << "cmp " << regs[12] << ",0" << endl;
						out << "jne " << ptr->rs1->l << endl;
						f = true;
					}
					if (ptr->rs2->type != vartype::nul)
					{
						if (f)
							out << "jmp " << ptr->rs2->l << endl;
						else
						{
							out << "cmp " << regs[12] << ",0" << endl;
							out << "je " << ptr->rs2->l << endl;
						}
					}
					continue;
				}
				if (Return *ptr = dynamic_cast<Return*>(*stmt))
				{
					var2reg(0, ptr->rs);
					out << "mov rsp,rbp" << endl;
					out << "pop rbp" << endl;
					out << "ret" << endl;
					continue;
				}
				if (Call *ptr = dynamic_cast<Call*>(*stmt))
				{
					int n = ptr->args.size();
					for (int i = 0; i < min(n, 6); i++)
					{
						var2reg(12, ptr->args[i]);
						out << "mov " << sixreg[i] << "," << regs[12] << endl;
					}
					if (n > 6)
						for (int i = 0; i < n - 6; i++)
						{
							var2reg(12, ptr->args[i + 6]);
							out << "push " << regs[12] << endl;
						}
					out << "call " << ptr->func->l << endl;
					if (n > 6)out << "add rsp," << 8 * (n - 6) << endl;
				}

			}
		}
	}
};
class smartgen
{
public:
	//unordered_map<string, vector<inst*>> st;
	IRtype IR;
	unordered_map<string, int> func2n;
	unordered_map<string, int> rspoff;

	vector<string> cstr;
	vector<int> cstr2reg;



	class allocator
	{
		smartgen* g;
		stringstream& c;
	public:
		int whouse[16];
		int next[16];
		int inf;
		allocator(smartgen* pg, stringstream& pc)
			:c(pc)
		{
			g = pg;
			clear();
		}
		void reset(int sz)
		{
			inf = sz;
			for (int i = 0; i < 16;i++)
			{
				whouse[i] = -1;
				next[i]=inf;
			}
		}
		void clear()
		{
			inf = -1;
			for (int i = 0; i < 16; i++)
				whouse[i] = next[i] = -1;
		}
		void free(int i)
		{
			whouse[i] = -1;
			next[i] = inf;
		}
		void spill(int k)
		{
			if (whouse[k] == -1 || whouse[k] == 0) return;
			regd d = g->getoff(whouse[k]);
			if(!d.f)
				c << "mov [" + g->posstr(d) + "]," + regs[k] << endl;
			whouse[k] = -1;
			next[k] = inf;
			return;
		}
		void fresh(int k)
		{
			if (whouse[k] == -1 || whouse[k] == 0) return;
			regd d = g->getoff(whouse[k]);
			if(!d.f)
				c << "mov [" + g->posstr(d) + "]," + regs[k] << endl;
			return;
		}
		void kill(int k)
		{
			whouse[k] = -1;
			next[k] = inf;
		}
		int alloc(int x)
		{
			int ret = -1;
			for (auto i : order)
				if (whouse[i] == -1)
					return i;
			int last = -1;
			for (auto i:order)
				if (next[i]>last)
				{
					last = next[i];
					ret = i;
				}
			spill(ret);
			return ret;
		}
		int casual(int x,int nuse,bool copy)
		{
			for (auto i:order)
				if (whouse[i] == x)
					return i;
			int ret = alloc(x);
			if (copy)
			{
				regd d = g->getoff(x);
				if (d.f)
					c << "mov " << regs[ret] << "," << d.c->l << endl;
				else
					c << "mov " + regs[ret] + ",[" + g->posstr(d) + "]" << endl;
			}
			whouse[ret] = x;
			next[ret] = nuse;
			return ret;
		}

		bool swp(int x, int want)
		{
			for (auto i:order)
				if (whouse[i] == x)
				{
					int temp = whouse[want];
					whouse[want] = whouse[i];
					whouse[i] = temp;

					temp = next[i];
					next[i] = next[want];
					next[want] = temp;

					c << "xchg " << regs[i] << "," << regs[want] << endl;
					return true;
				}
			return false;
		}

		void force(var *x, int nuse, int to)
		{
			if (x->type == imm)
			{
				spill(to);
				c << "mov " << regs[to] << "," << x->val << endl;
				next[to] = inf;
				return;
			}
			if (swp(x->r->id, to))
			{
				next[to] = nuse;
				return;
			}
			spill(to);
			next[to] = nuse;
			assert(x->type == regist);
			whouse[to] = x->r->id;
			regd d = g->getoff(x->r->id);
			if (d.f)
				c << "mov " << regs[to] << "," << d.c->l << endl;
			else
				c << "mov " << regs[to] << ",[rbp-" << to_string(d.off) << "]" << endl;
			return;
		}

		void forfunc(var *x, int nuse, int to)
		{
			if (x->type == imm)
			{
				spill(to);
				c << "mov " << regs[to] << "," << x->val << endl;
				next[to] = inf;
				return;
			}
			int rid = whe(x->r->id);
			if (rid != -1)
			{
				if (to == rid) return;
				spill(to);
				c << "mov " << regs[to] << "," << regs[rid] << endl;
				next[to] = nuse;
				return;
			}
			spill(to);
			next[to] = nuse;
			assert(x->type == regist);
			whouse[to] = x->r->id;
			regd d = g->getoff(x->r->id);
			if (d.f)
				c << "mov " << regs[to] << "," << d.c->l << endl;
			else
				c << "mov " << regs[to] << ",[rbp-" << to_string(d.off) << "]" << endl;
		}

		int whe(int x)
		{
			for (auto i:order)
				if (whouse[i] == x)
					return i;
			return -1;
		}
	};
	
	allocator allo;
	struct cons
	{
		int voff;
		string l;
		cons(int poff, const string & pl)
			:voff(poff), l(pl)
		{}
	};
	class regd
	{
	public:
		int off;
		cons *c;
		bool f;//true if it's const
		regd(int poff = 0, cons *pc = nullptr, bool pf = false)
			:off(poff), f(pf), c(pc)
		{}
	};

	string posstr(const regd & d)
	{
		if (d.f)
		{
			if(d.c->voff == 0)
				return d.c->l;
		}
		assert(!d.f);
		return "rbp-" + to_string(d.off);
	}
	map<int, int> reg2off;
	map<int, cons*> freg;
	map<int, cons*> bfreg;
	int offset;
	static const string sixreg[6];
	static const string regs[16];
	static const vector<int> order;
	static const vector<int> sixid;
	static const vector<int> notsixid;
	static const string regsb[16];
	stringstream code;
	smartgen(const IRgen & pIR, const IRtype & IRcode)
		:func2n(pIR.func2n), cstr(pIR.cstr), cstr2reg(pIR.cstr2reg), offset(0), IR(pIR.IR), allo(this, code)
	{
	}
	regd getoff(int x)
	{
		if (reg2off.count(x)) return regd(reg2off[x], nullptr, false);
		if (freg.count(x)) return regd(0, freg[x], true);
		throw "there";
		return 0;
	}
	void givepos(var *v)
	{
		if (v->type != vartype::regist) return;
		int id = v->r->id;
		if (reg2off.count(id) || freg.count(id)) return;
		offset += 8;
		reg2off[id] = offset;
		return;
	}
	vector<int> strans(string x)
	{
		vector<int> r;
		r.push_back(0);
		int l = 0;
		for (int i = 0; i < x.length(); i++)
		{
			if (x[i] != '\\') r.push_back(x[i]);
			else
			{
				++i;
				if (x[i] == 'n') r.push_back(10);
				if (x[i] == '\\') r.push_back(92);
				if (x[i] == 't') r.push_back(9);
				if (x[i] == '\"') r.push_back(34);
			}
			++l;
		}
		r.front() = l;
		return r;
	}
	void prework()
	{
		ifstream h("head.txt");
		string temp;
		while (h.good())
		{
			getline(h, temp);
			code << temp << endl;
		}
		h.close();
		auto& fcs = IR.fcs;
		auto g = fcs["global"].bbs;
		{
			int id = 0;
			//auto gl = g.front().stmt;
			code << "section .bss" << endl;
			for (auto gl : g)
				for (auto stmt : gl.stmt)
				{
					if (delc *ptr = dynamic_cast<delc*>(stmt))
					{
						code << "gv" << id << ":" << endl;
						code << "resb 8" << endl;
						bfreg[ptr->r->r->id] = new cons(0, "gv" + to_string(id));
						++id;
					}
				}
			code << endl;
		}

		code << "section .data" << endl;
		for (int i = 0; i < cstr.size(); i++)
		{
			vector<int> r = strans(cstr[i]);
			code << "dq " << r.front() << endl;
			code << "_cstr" << i << ":" << endl << "db ";
			for (int i = 1; i < r.size(); i++)
				code << r[i] << ",";
			code << "0";
			code << endl << endl;
			bfreg[cstr2reg[i]] = new cons(0, "_cstr" + to_string(i));
		}
	}

	void prefunc(const string & fn, const vector<inst*> fc)
	{
		reg2off.clear();
		freg = bfreg;
		//vector<var*> worklist;
		bool isglobal = false;
		if (fn == "global") isglobal = true;
		offset = 0;
		for (auto stmt : fc)
		{
			if (delc *ptr = dynamic_cast<delc*>(stmt))
			{
				if (isglobal) continue;
				//offset += 8;
				if (ptr->v) givepos(ptr->v);
				freg[ptr->r->r->id] = new cons(offset, string());
				continue;
			}
			if (bincalc *ptr = dynamic_cast<bincalc*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Not *ptr = dynamic_cast<Not*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Neg *ptr = dynamic_cast<Neg*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Rev *ptr = dynamic_cast<Rev*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Load *ptr = dynamic_cast<Load*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
			if (Move *ptr = dynamic_cast<Move*>(stmt))
			{
				givepos(ptr->rd);
				continue;
			}
		}
		/*for (auto v : worklist)
			givepos(v);*/
		offset -= 8 * func2n[fn];
		if (offset % 16 != 0) offset += 8;
		rspoff[fn] = offset;
	}

	bool is_contained(int id, var *v)
	{
		if (v->type != regist) return false;
		return v->r->id == id;
	}

	int findnext(int id, const vector<inst*>& stmts, int now)
	{
		int i = now + 1;
		for (; i < stmts.size(); i++)
		{
			auto stmt = stmts[i];
			if (bincalc *ptr = dynamic_cast<bincalc*>(stmt))
			{
				if (is_contained(id, ptr->rs1) || is_contained(id, ptr->rs2) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Not *ptr = dynamic_cast<Not*>(stmt))
			{
				if (is_contained(id, ptr->rs) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Neg *ptr = dynamic_cast<Neg*>(stmt))
			{
				if (is_contained(id, ptr->rs) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Rev *ptr = dynamic_cast<Rev*>(stmt))
			{
				if (is_contained(id, ptr->rs) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Load *ptr = dynamic_cast<Load*>(stmt))
			{
				if (is_contained(id, ptr->rs1) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Move *ptr = dynamic_cast<Move*>(stmt))
			{
				if (is_contained(id, ptr->rs) || is_contained(id, ptr->rd))
					return i;
				continue;
			}
			if (Store *ptr = dynamic_cast<Store*>(stmt))
			{
				if (is_contained(id, ptr->rd1) || is_contained(id, ptr->rs))
					return i;
				continue;
			}
			if (Branch *ptr = dynamic_cast<Branch*>(stmt))
			{
				if (is_contained(id, ptr->rsc))
					return i;
				continue;
			}
			if (Jump *ptr = dynamic_cast<Jump*>(stmt))
			{
				if (is_contained(id, ptr->rs))
					return i;
				continue;
			}
			if (Return *ptr = dynamic_cast<Return*>(stmt))
			{
				if (is_contained(id, ptr->rs))
					return i;
				continue;
			}
			if (Call *ptr = dynamic_cast<Call*>(stmt))
			{
				for (auto v : ptr->args)
					if (is_contained(id, v))
						return i;
				continue;
			}
		}
		return i;
	}
	template<typename T>
	void bintrans(T* ptr, const string & op,int i,const vector<inst*> stmts)
	{
		auto rs1 = ptr->rs1, rs2 = ptr->rs2;
		int regrd = allo.casual(ptr->rd->r->id, i, false);
		bool isimm1 = rs1->type == imm, isimm2 = rs2->type == imm;
		int regrs1, regrs2;
		if (!isimm1) regrs1 = allo.casual(rs1->r->id, i, true);
		if (!isimm2) regrs2 = allo.casual(rs2->r->id, i, true);
		if (isimm1 && isimm2)
		{
			long long v1 = rs1->val, v2 = rs2->val;
			long long result;
			if (op == "add")
				result = v1 + v2;
			if (op == "sub")
				result = v1 - v2;
			if (op == "imul")
				result = v1 * v2;
			if (op == "and")
				result = v1&v2;
			if (op == "or")
				result = v1 | v2;
			if (op == "xor")
				result = v1^v2;
			code << "mov " << regs[regrd] << "," << result << endl;
		}
		else if (isimm1)
		{
			if (regrd == regrs2)
			{
				if (op != "sub")
					code << op << " " << regs[regrd] << "," << ptr->rs1->val << endl;
				else
				{
					code << "neg " << regs[regrd] << endl;
					code << "add " << regs[regrd] << "," << ptr->rs1->val << endl;
				}
			}
			else
			{
				code << "mov " << regs[regrd] << "," << ptr->rs1->val << endl;
				code << op << " " << regs[regrd] << "," << regs[regrs2] << endl;
			}
			allo.next[regrs2] = findnext(rs2->r->id, stmts, i);
		}
		else if (isimm2)
		{
			code << "mov " << regs[regrd] << "," << regs[regrs1] << endl;
			bool opt = false;
			if (op == "imul")
			{
				if (ptr->rs2->val == 2) code << "sal " << regs[regrd] << ",1" << endl, opt = true;
				if (ptr->rs2->val == 4) code << "sal " << regs[regrd] << ",2" << endl, opt = true;
				if (ptr->rs2->val == 8) code << "sal " << regs[regrd] << ",3" << endl, opt = true;
			}
			if(!opt) code << op << " " << regs[regrd] << "," << ptr->rs2->val << endl;
			allo.next[regrs1] = findnext(rs1->r->id, stmts, i);
		}
		else
		{
			if (regrd == regrs2)
			{
				if (op != "sub")
					code << op << " " << regs[regrd] << "," << regs[regrs1] << endl;
				else
				{
					code << "neg " << regs[regrd] << endl;
					code << "add " << regs[regrd] << "," << regs[regrs1] << endl;
				}
			}
			else
			{
				code << "mov " << regs[regrd] << "," << regs[regrs1] << endl;
				code << op << " " << regs[regrd] << "," << regs[regrs2] << endl;
			}
			allo.next[regrs1] = findnext(rs1->r->id, stmts, i);
			allo.next[regrs2] = findnext(rs2->r->id, stmts, i);
		}
		allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
	}

	template<typename T>
	void cmptrans(T* ptr, const string & op, int i, const vector<inst*> stmts)
	{
		auto rs1 = ptr->rs1, rs2 = ptr->rs2;
		int regrd = allo.casual(ptr->rd->r->id, i, false);
		bool isimm1 = rs1->type == imm, isimm2 = rs2->type == imm;
		int regrs1, regrs2;
		if (!isimm1) regrs1 = allo.casual(rs1->r->id, i, true);
		if (!isimm2) regrs2 = allo.casual(rs2->r->id, i, true);
		if (isimm1 && isimm2)
		{
			long long v1 = rs1->val, v2 = rs2->val;
			long long result;
			if (op == "e")
				result = v1 == v2;
			if (op == "l")
				result = v1<v2;
			if (op == "le")
				result = v1 <= v2;
			if (op == "g")
				result = v1 > v2;
			if (op == "ge")
				result = v1 >= v2;
			if (op == "ne")
				result = v1 != v2;
			code << "mov " << regs[regrd] << "," << result << endl;
		}
		else if (isimm1)
		{
			code << "cmp " << ptr->rs1->val << "," << regs[regrs2] << endl;
			//code << "xor " << regs[regrd] << "," << regs[regrd] << endl;
			code << "set" + op << " " << regsb[regrd] << endl;
			allo.next[regrs2] = findnext(rs2->r->id, stmts, i);
		}
		else if (isimm2)
		{
			code << "cmp " << regs[regrs1] << "," << ptr->rs2->val << endl;
			//code << "xor " << regs[regrd] << "," << regs[regrd] << endl;
			code << "set" + op << " " << regsb[regrd] << endl;
			allo.next[regrs1] = findnext(rs1->r->id, stmts, i);
		}
		else
		{
			code << "cmp " << regs[regrs1] << "," << regs[regrs2] << endl;
			//code << "xor " << regs[regrd] << "," << regs[regrd] << endl;
			code << "set" + op << " " << regsb[regrd] << endl;
			allo.next[regrs1] = findnext(rs1->r->id, stmts, i);
			allo.next[regrs2] = findnext(rs2->r->id, stmts, i);
		}
		allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
	}

	template<typename T>
	void unitrans(T* ptr, const string & op, int i, const vector<inst*> stmts)
	{
		auto rs = ptr->rs;
		int regrd = allo.casual(ptr->rd->r->id, i, false);
		bool isimm = rs->type == imm;
		int regrs;
		if (!isimm) regrs = allo.casual(rs->r->id, i, true);
		if (isimm)
		{
			long long v = rs->val;
			long long result;
			if (op == "rev")
				result = ~v;
			if (op == "neg")
				result = -v;
			if (op == "not")
				result = v ^ 1;
			code << "mov " << regs[regrd] << "," << result << endl;
		}
		else
		{
			if (op == "rev")
			{
				code << "mov " << regs[regrd] << "," << regs[regrs] << endl;
				code << "not " << regs[regrd] << endl;
			}
			else if (op == "neg")
			{
				code << "mov " << regs[regrd] << "," << regs[regrs] << endl;
				code << "neg " << regs[regrd] << endl;
			}
			else
			{
				code << "mov " << regs[regrd] << "," << regs[regrs] << endl;
				code << "xor " << regs[regrd] << ",1" << endl;
			}
			allo.next[regrs] = findnext(rs->r->id, stmts, i);
		}
		allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);

	}
	void transblock(const basicblock & bb)
	{
		auto& stmts = bb.stmt;
		allo.reset(stmts.size());
		for (int i = 0; i < stmts.size();i++)
		{
			auto stmt = stmts[i];
			if (lab *ptr = dynamic_cast<lab*>(stmt))
			{
				code << ptr->content() << endl;
				continue;
			}
			if (Add *ptr = dynamic_cast<Add*>(stmt))
			{
				bintrans<Add>(ptr, "add",i,stmts);
				continue;
			}
			if (Sub *ptr = dynamic_cast<Sub*>(stmt))
			{
				bintrans<Sub>(ptr, "sub",i,stmts);
				continue;
			}
			if (Mult *ptr = dynamic_cast<Mult*>(stmt))
			{
				bintrans<Mult>(ptr, "imul", i, stmts);
				continue;
			}
			if (And *ptr = dynamic_cast<And*>(stmt))
			{
				bintrans<And>(ptr, "and", i, stmts);
				continue;
			}
			if (Or *ptr = dynamic_cast<Or*>(stmt))
			{
				bintrans<Or>(ptr, "or", i, stmts);
				continue;
			}
			if (Xor *ptr = dynamic_cast<Xor*>(stmt))
			{
				bintrans<Xor>(ptr, "xor", i, stmts);
				continue;
			}
			if (cmp_EQ *ptr = dynamic_cast<cmp_EQ*>(stmt))
			{
				cmptrans<cmp_EQ>(ptr, "e", i, stmts);
				continue;
			}
			if (cmp_GE *ptr = dynamic_cast<cmp_GE*>(stmt))
			{
				cmptrans<cmp_GE>(ptr, "ge", i, stmts);
				continue;
			}
			if (cmp_GT *ptr = dynamic_cast<cmp_GT*>(stmt))
			{
				cmptrans<cmp_GT>(ptr, "g", i, stmts);
				continue;
			}
			if (cmp_LE *ptr = dynamic_cast<cmp_LE*>(stmt))
			{
				cmptrans<cmp_LE>(ptr, "le", i, stmts);
				continue;
			}
			if (cmp_LT *ptr = dynamic_cast<cmp_LT*>(stmt))
			{
				cmptrans<cmp_LT>(ptr, "l", i, stmts);
				continue;
			}
			if (cmp_NE *ptr = dynamic_cast<cmp_NE*>(stmt))
			{
				cmptrans<cmp_NE>(ptr, "ne", i, stmts);
				continue;
			}
			if (Not *ptr = dynamic_cast<Not*>(stmt))
			{
				unitrans(ptr, "not", i, stmts);
				continue;
			}
			if (Neg *ptr = dynamic_cast<Neg*>(stmt))
			{
				unitrans(ptr, "neg", i, stmts);
				continue;
			}
			if (Rev *ptr = dynamic_cast<Rev*>(stmt))
			{
				unitrans(ptr, "rev", i, stmts);
				continue;
			}
			if (Return* ptr = dynamic_cast<Return*>(stmt))
			{
				if (ptr->rs->type != nop)
				{
					if (ptr->rs->type == imm)
						code << "mov rax," << ptr->rs->val << endl;
					else
					{
						int regrs = allo.casual(ptr->rs->r->id, i, true);
						//maybe modify next...
						code << "mov rax," << regs[regrs] << endl;
					}
				}
				code << "mov rsp,rbp" << endl;
				code << "pop rbp" << endl;
				code << "ret" << endl;
				continue;
			}
			if (Branch* ptr = dynamic_cast<Branch*>(stmt))
			{
				if (ptr->rsc->type == imm)
				{
					if (ptr->rsc->val == 1)
						code << "jmp " << ptr->rs1->l << endl;
					else code << "jmp " << ptr->rs2->l << endl;
				}
				else
				{
					int regrs = allo.casual(ptr->rsc->r->id, i, true);
					code << "cmp " << regsb[regrs] << ",0" << endl;
					allo.next[regrs] = findnext(ptr->rsc->r->id, stmts, i);
				}
				for (auto i : order)
					allo.spill(i);
				code << "jnz " << ptr->rs1->l << endl;
				code << "jmp " << ptr->rs2->l << endl;
				continue;
			}
			if (Jump* ptr = dynamic_cast<Jump*>(stmt))
			{
				for (auto i : order)
					allo.spill(i);
				code << "jmp " << ptr->rs->l << endl;
				continue;
			}
			if (Load* ptr = dynamic_cast<Load*>(stmt))
			{
				int regrd = allo.casual(ptr->rd->r->id,i,false);
				regd d = getoff(ptr->rs1->r->id);
				if (d.f)
				{
					if (d.c->voff)
						code << "mov " << regs[regrd] << ",[rbp-" << d.c->voff << "]" << endl;
					else
						code << "mov " << regs[regrd] << ",[" << posstr(d) << "]" << endl;
				}
				else
				{
					int regrs = allo.casual(ptr->rs1->r->id, i, true);
					code << "mov " << regs[regrd] << ",[" << regs[regrs] << "]" << endl;
					allo.next[regrs] = findnext(ptr->rs1->r->id, stmts, i);
				}
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
				continue;
			}
			if (Store* ptr = dynamic_cast<Store*>(stmt))
			{
				int regrs = -1;
				string v;
				if (ptr->rs->type == imm)
					v = to_string(ptr->rs->val);
				else
				{
					regrs = allo.casual(ptr->rs->r->id, i, true);
					v = regs[regrs];
				}
				regd d = getoff(ptr->rd1->r->id);
				if (d.f)
					code << "mov qword[" << posstr(d) << "]," << v << endl;
				else
				{
					int regrd = allo.casual(ptr->rd1->r->id, i, true);
					code << "mov qword[" << regs[regrd] << "]," << v << endl;
					allo.next[regrd] = findnext(ptr->rd1->r->id, stmts, i);
				}
				if (regrs != -1)
					allo.next[regrs] = findnext(ptr->rs->r->id, stmts, i);
				continue;
			}
			if (Move* ptr = dynamic_cast<Move*>(stmt))
			{
				int regrd = allo.casual(ptr->rd->r->id, i, false);
				if (ptr->rs->type == imm)
					code << "mov " << regs[regrd] << "," << ptr->rs->val << endl;
				else
				{
					regd d;
					bool flag = ptr->rs->r->id != 0;
					if (!flag)
						code << "mov " << regs[regrd] << ",rax" << endl;
					else if (flag)
					{
						d = getoff(ptr->rs->r->id);
						if (d.f)
						{
							if (d.c->voff)
								assert(false);
							else
								code << "mov " << regs[regrd] << "," << d.c->l << endl;
						}
						else
						{
							int regrs = allo.casual(ptr->rs->r->id, i, true);
							code << "mov " << regs[regrd] << "," << regs[regrs] << endl;
							allo.next[regrs] = findnext(ptr->rs->r->id, stmts, i);
						}
					}
				}
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
				
				continue;
			}
			if (Div* ptr = dynamic_cast<Div*>(stmt))
			{
				allo.force(ptr->rs1, i, 0);
				allo.spill(2);
				code << "cqo" << endl;
				if (ptr->rs2->type == imm)
				{
					allo.force(ptr->rs2, i, 1);
					allo.fresh(0);
					code << "idiv rcx" << endl;
					allo.kill(0);
					allo.kill(2);
				}
				else
				{
					int regrs = allo.casual(ptr->rs2->r->id, i, true);
					allo.fresh(0);
					code << "idiv " << regs[regrs] << endl;
					allo.kill(0);
					allo.kill(2);
					if (regrs != 2)
						allo.next[regrs] = findnext(ptr->rs2->r->id, stmts, i);
				}
				int regrd = allo.casual(ptr->rd->r->id, i, false);
				code << "mov " << regs[regrd] << ",rax" << endl;
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
				
				continue;
			}
			if (Mod* ptr = dynamic_cast<Mod*>(stmt))
			{
				allo.force(ptr->rs1, i, 0);
				allo.spill(2);
				code << "cqo" << endl;
				if (ptr->rs2->type == imm)
				{
					allo.force(ptr->rs2, i, 1);
					allo.fresh(0);
					code << "idiv rcx" << endl;
					allo.kill(0);
					allo.kill(2);
				}
				else
				{
					int regrs = allo.casual(ptr->rs2->r->id, i, true);
					allo.fresh(0);
					code << "idiv " << regs[regrs] << endl;
					allo.kill(0);
					allo.kill(2);
					if (regrs != 2)
						allo.next[regrs] = findnext(ptr->rs2->r->id, stmts, i);
				}
				int regrd = allo.casual(ptr->rd->r->id, i, false);
				code << "mov " << regs[regrd] << ",rdx" << endl;
				
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
				
				continue;
			}
			if (Lshift *ptr = dynamic_cast<Lshift*>(stmt))
			{
				int regrd = allo.casual(ptr->rd->r->id, i, true);
				allo.spill(2);
				if (ptr->rs2->type == imm)
					code << "mov rcx," << ptr->rs2->val << endl;
				else
				{
					int rid = allo.whe(ptr->rs2->r->id);
					if (rid != -1)
						code << "mov rcx," << regs[rid] << endl;
					else
					{
						regd d = getoff(ptr->rs2->r->id);
						code << "mov rcx,[rbp-" << d.off << "]" << endl;
					}
				}
				if (ptr->rs1->type == imm)
					code << "mov " << regs[regrd] << "," << ptr->rs1->val << endl;
				else
				{
					int regrs1 = allo.casual(ptr->rs1->r->id, i, true);
					code << "mov " << regs[regrd] << "," << regs[regrs1] << endl;
					allo.next[regrs1] = findnext(ptr->rs1->r->id, stmts, i);
				}
				code << "sal " << regs[regrd] << ",cl" << endl;
				allo.kill(2);
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
			}
			if (Rshift *ptr = dynamic_cast<Rshift*>(stmt))
			{
				int regrd = allo.casual(ptr->rd->r->id, i, true);
				allo.spill(2);
				if (ptr->rs2->type == imm)
					code << "mov rcx," << ptr->rs2->val << endl;
				else
				{
					int rid = allo.whe(ptr->rs2->r->id);
					if (rid != -1)
						code << "mov rcx," << regs[rid] << endl;
					else
					{
						regd d = getoff(ptr->rs2->r->id);
						code << "mov rcx,[rbp-" << d.off << "]" << endl;
					}
				}
				if (ptr->rs1->type == imm)
					code << "mov " << regs[regrd] << "," << ptr->rs1->val << endl;
				else
				{
					int regrs1 = allo.casual(ptr->rs1->r->id, i, true);
					code << "mov " << regs[regrd] << "," << regs[regrs1] << endl;
					allo.next[regrs1] = findnext(ptr->rs1->r->id, stmts, i);
				}
				code << "sar " << regs[regrd] << ",cl" << endl;
				allo.kill(2);
				allo.next[regrd] = findnext(ptr->rd->r->id, stmts, i);
			}
			if (Call *ptr = dynamic_cast<Call*>(stmt))
			{
				int sz = ptr->args.size();
				for (int i = 6; i < sz; i++)
				{
					var *v = ptr->args[i];
					if (v->type == imm)
						code << "push " << v->val << endl;
					else
					{
						int regrs = allo.whe(v->r->id);
						if (regrs != -1)
							code << "push " << regs[regrs] << endl;
						else
						{
							regd d = getoff(v->r->id);
							if (d.f)
								code << "push " << d.c->l << endl;
							else
								code << "push " << "qword[rbp-" << d.off << "]" << endl;
						}
					}
				}
				/*for (auto i : order)
					allo.spill(i);*/
				for (int i = 0; i < 6; i++)
					if (i < sz)
						allo.force(ptr->args[i], i, sixid[i]);
				for (int i = 0; i < 6; i++)
					allo.fresh(sixid[i]);
				for (auto i : notsixid)
					allo.spill(i);
				code << "call " << ptr->func->l << endl;
				for (auto i : sixid)
					allo.kill(i);
				allo.whouse[0] = 0;
				allo.next[0] = findnext(0, stmts, i);
				continue;
			}
		}
	}
	void trans()
	{
		prework();
		for (auto fc : IR.fcs)
		{
			vector<inst*> stmt;
			for (auto bb : fc.second.bbs)
				for (auto st : bb.stmt)
					stmt.push_back(st);
			prefunc(fc.first, stmt);
			code << "section .text" << endl;
			code << ";" + fc.first << endl;
			auto& sth = fc.second.bbs.front().stmt;
			auto fclbl = sth.front();
			code << fclbl->content() << endl;
			sth.erase(sth.begin());
			code << "push rbp" << endl;
			code << "mov rbp,rsp" << endl;
			int n = func2n[fc.first];
			for (int i = 0; i < min(n, 6); i++)
				code << "push " + sixreg[i] << endl;
			if (n > 6)
				for (int i = 0; i < n - 6; i++)
					code << "push qword[rbp+" << (n - 5 - i) * 8 << ']' << endl;
			code << "sub rsp," << rspoff[fc.first] << endl;

			auto& bbs = fc.second.bbs;
			int sz = bbs.size();
			int *v = new int[sz];
			int *stack = new int[sz];
			int top = 0;
			for (int i = 0; i < sz; i++) v[i] = 0;
			stack[top++] = 0;
			v[0] = 1;
			while (top)
			{
				int index = stack[--top];
				auto& bb = bbs[index];
				transblock(bb);
				for (auto next : bb.to)
					if (!v[next])
					{
						v[next] = 1;
						stack[top++] = next;
					}
			}
		}
	}
};
