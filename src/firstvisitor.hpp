#pragma once
#include <complierBaseVisitor.h>
#include<iostream>
#include<unordered_map>
#include<vector>
using namespace std;
struct functype
{
	string retype;
	vector<string> paratype;
};
class firstvisitor : public complierBaseVisitor
{
	string prefix;
	bool isinclass;
public:
	bool result;
public:
	std::unordered_map<std::string, int> classes;
	std::unordered_map<std::string, functype> funcs;
	std::unordered_map <std::string, std::string> name2type;
public:
	firstvisitor()
	{
		prefix = "";
		isinclass = false;
		result = true;
		functype temp;
		temp.retype = "void";
		temp.paratype.push_back("string");
		funcs["print"] = temp;
		funcs["println"] = temp;
		temp.retype = "string";
		temp.paratype.clear();
		funcs["getString"] = temp;
		temp.retype = "int";
		funcs["getInt"] = temp;
		temp.retype = "string";
		temp.paratype.push_back("int");
		funcs["toString"] = temp;
		temp.retype = "int";
		funcs["string.ord"] = temp;
		temp.paratype.clear();
		funcs["array.size"] = temp;
		funcs["string.length"] = temp;
		funcs["string.parseInt"] = temp;
		temp.retype = "string";
		temp.paratype.push_back("int");
		temp.paratype.push_back("int");
		funcs["string.substring"] = temp;
		for (auto i : funcs)
			name2type[i.first] = "func";
	}
	virtual antlrcpp::Any visitDelcfunc(complierParser::DelcfuncContext *ctx) override 
	{
		string name = (prefix.size() ? (prefix + ".") : "") + ctx->ID()->getText();
		if (funcs.count(name) | name2type.count(name))
			cout << "has been  defined\n" << ctx->getText() << endl, result = false;
		else
		{
			functype f;
			f.retype = ctx->type()->getText();
			for (auto iter : ctx->delc_only())
				f.paratype.push_back(iter->type()->getText());
			funcs[name] = f;
			name2type[name] = string("func");
		}
		/*for (auto iter : ctx->stat())
			visit(iter);*/
		return 0;
	}

	virtual antlrcpp::Any visitDelcclass(complierParser::DelcclassContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		if (classes.count(name) | name2type.count(name))
		{
			cout << "error:has been defined\n" << ctx->getText() << endl, result = false;
			return visitChildren(ctx);
		}
		else
		{
			classes[name] = 1;
			name2type[name] = "class";
			prefix = name;
			//isinclass = true;
			antlrcpp::Any ret = visit(ctx->classbody());
			//isinclass = false;
			prefix = "";
			return ret;
		}
	}

	virtual antlrcpp::Any visitDelc_only(complierParser::Delc_onlyContext *ctx) override
	{
		string name = (prefix.size() ? (prefix + ".") : "") + ctx->ID()->getText();
		if (name2type.count(name) | funcs.count(name))
			cout << "error:has been defined\n" << ctx->getText() << endl, result = false;
		else
			name2type[name] = ctx->type()->getText();
		return visitChildren(ctx);
	}
	virtual antlrcpp::Any visitDelc_cons_func(complierParser::Delc_cons_funcContext *ctx) override 
	{
		string name = ctx->ID()->getText();
		if (name != prefix)
			cout << "error:name not same\n" << ctx->getText() << endl, result = false;
		else
		{
			name = prefix + ".$" + name;
			if (funcs.count(name) | name2type.count(name))
				cout << "error:has been defined\n" << ctx->getText() << endl, result = false;
			else
			{
				functype f;
				f.retype = "void";
				funcs[name] = f;
				name2type[name] = "func";
			}
		}
		return 0;
	}
};